# Universal Beginning Code

# Tested Environment: Python version 3.7.11, Geopandas version 0.9.0

# GENERAL IMPORTS

import geopandas as gpd
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# MODEL IMPORTS

from sklearn.linear_model import Lasso # linear and multi-linear regression model fitting
from sklearn.preprocessing import OneHotEncoder, StandardScaler # OneHotEncoding, Normalization / Feature Scaling

import statsmodels.api as sm
import statsmodels.formula.api as smf


# COLUMN FILTERING FUNCTION (preventing crashes)

def df_column_filtering_fadn(filename, fadn_variables_list):
    
    empty_columns_list = []
    country_df_fadn = pd.read_csv(filename)
    country_df_fadn = df_fadn_regional_filtering(filename, country_df_fadn)

    for variable in fadn_variables_list:
        if (country_df_fadn[variable].min() == 0.0) & (country_df_fadn[variable].max() == 0.0): #if column only contains 0 float values...
            empty_columns_list += [variable] # ... eliminate the column from the remainder of the analysis
        elif len(country_df_fadn[variable]) == 0: #need to filter out empty columns with no 0 values but all 'nan's etc... a column of all nans has length = 0 so harnessing this property
            empty_columns_list += [variable] # ... eliminate the column from the remainder of the analysis
    
    print("Empty/nan variable columns in FADN region data, to be removed from analysis:",empty_columns_list) #output readability
    
    fadn_variables_list_filtered = [x for x in fadn_variables_list if x not in empty_columns_list] #generate new list of columns
    
    return fadn_variables_list_filtered


# WEIGHTING FUNCTION

def df_transform_to_weighted_farms_rep(input_df): # function to transform dataframe into new dataframe with 1 duplicate row for each 'farms represented' from each FADN row
    
    old_df = input_df.copy().reset_index()
    old_df['SYS02'] = [round(x,0) for x in old_df['SYS02']] #round to nearest integer, up or down
    new_df = pd.DataFrame(columns=old_df.columns) #declares in right df shape with columns from old df

    row_i = 0
    for row in range(0,len(old_df)): #for each row in original dataframe
        row_values_list = list(old_df.iloc[row,]) #efficiency
        for i in range(0,int(old_df['SYS02'][row])): #for each time a farm is represented for that row # int casting causing forced 'rounding down' e.g. 3.8 => 3 (with exceptions for float values appearing to represent recurring digital points).
            row_i += 1
            new_df.loc[row_i] = row_values_list #assign a duplicate row into the new dataframe with the same values

    return new_df


# CODE TO DETERMINE MULTICOLLINEARITY EXCLUSIONS AUTOMATICALLY

# excludes at least one variable if the absolute correlation coefficient (r value) exceeds 0.8 between two variables according to corr() function in pandas
# excludes the variable with lower direct R^2 value to the dependant variable - here 'SE010' labour

def return_unilateral_ols_score(variable, fadn_df_rep):
    trial_feature = str(variable)
    data_cs = fadn_df_rep[[trial_feature] + ['SE010']]
    X = data_cs[trial_feature]
    y = data_cs['SE010']
    X = sm.add_constant(X)
    ols_est = sm.OLS(y, X).fit()
    return ols_est.rsquared #https://stackoverflow.com/questions/48522609/how-to-retrieve-model-estimates-from-statsmodels


def looped_optimisation_collinearity(fadn_df_rep, set_of_original_variables):    
    #LOOPED FROM HERE ON IN UNTIL NO CONFLICTS REMAIN (can save more variables to stay in the model than just eliminating all conflicting variables at once at first)
    i = 0
    while True:
        
        should_go_list = []
        
        i += 1
        if i == 1: #first iteration
            set_of_original_variables.remove('SYS02') #removes ['SYS02'] ahead of correlations evaluations - see https://www.w3schools.com/python/python_lists_remove.asp
        
        fadn_df = fadn_df_rep[set_of_original_variables] #only continuous will appear in correlation matrix and thus be filtered out using this process
        fadn_df_correlations = fadn_df.copy().corr() #generates correlation matrix df
        
        # comb through correlation results to find pairs to evaluate
        list_of_variable_pairings_to_evaluate = []
        for variable in set_of_original_variables:
            if len(fadn_df_correlations.loc[abs(fadn_df_correlations[variable]) > 0.8 ]) > 1:
                variables_list_to_inspect = list(fadn_df_correlations.loc[abs(fadn_df_correlations[variable]) > 0.8 ].index.values)
                list_of_variable_pairings_to_evaluate += [variables_list_to_inspect]
        
        unique_pairings = [list(x) for x in set(tuple(x) for x in list_of_variable_pairings_to_evaluate)] #https://stackoverflow.com/questions/3724551/python-uniqueness-for-list-of-lists

        for nested_list in unique_pairings:
            if len(nested_list) > 1:
                variable_ols_r2_scores_dict = {}
                for variable in nested_list:
                    variable_ols_r2_scores_dict[variable] = return_unilateral_ols_score(variable, fadn_df_rep)
                should_go_list += [x for x in nested_list if variable_ols_r2_scores_dict[x] != max(variable_ols_r2_scores_dict.values())] #all but the highest R2 variable go
        
        if len(should_go_list) == 0:
            break
            
        should_go_list = list(set(should_go_list)) #make unique values only to stop repeat entries
        
        set_of_original_variables = [x for x in set_of_original_variables if x not in should_go_list]
        

    set_of_independent_evaluated_variables = set_of_original_variables
    return set_of_independent_evaluated_variables


def weighted_df_for_correlations_generator(filename):
    
    fadn_df = pd.read_csv(filename) #read in data
    
    #regional filtering
    fadn_df = df_fadn_regional_filtering(filename, fadn_df)
    
    # deploy weighting function to consider 'SYS02' value in regressions
    fadn_df_rep = df_transform_to_weighted_farms_rep(fadn_df).reset_index()

    return fadn_df_rep


# LASSO FUNCTIONS

def FADN_lasso_all(filename):
    
    global fadn_df_rep #to avoid repeat calculation after calculating again here
    
    fadn_df = pd.read_csv(filename) #read in data
    fadn_df = df_fadn_regional_filtering(filename, fadn_df)
    
    fadn_df_rep = df_transform_to_weighted_farms_rep(fadn_df).reset_index() # deploy weighting function to consider 'SYS02' value in regressions
    
    all_variables = ['SE010'] + set_of_independent_evaluated_variables + ['SYS02']
    
    fadn_df = fadn_df_rep[all_variables] #'SYS02' is 'farms represented'

    # subsequent code written with guidance from URL: https://www.kaggle.com/thaddeussegura/enough-to-be-dangerous-multiple-linear-regression
    X = fadn_df.iloc[:,1:-1] #.values for np array
    Y = fadn_df.iloc[:,0] #.values for np array

    # https://stackoverflow.com/questions/43798377/one-hot-encode-categorical-variables-and-scale-continuous-ones-simultaneouely
    columns_to_be_scaled = [x for x in set_of_independent_evaluated_variables if x not in ['AGE','TF8','SEX','REGION','ALTITUDE','ANC']] #continuous
    columns_to_be_encoded = ['AGE','TF8','SEX','REGION','ALTITUDE','ANC'] #categorical

    scaled_columns = StandardScaler().fit_transform(X[columns_to_be_scaled])
    encoded_columns = OneHotEncoder(sparse=False).fit_transform(X[columns_to_be_encoded])

    X_scaled_encoded = np.concatenate([scaled_columns, encoded_columns], axis = 1)
    
    # Guided by tutorial available at URL: https://machinelearningmastery.com/lasso-regression-with-python/
    model_lasso_all = Lasso(alpha=1.0)
    model_lasso_all.fit(X_scaled_encoded, Y)

    r2_score_lasso_reg_all = model_lasso_all.score(X_scaled_encoded, Y)
    print("\nLASSO REGRESSION FUNCTION - All longlisted variables from " + filename + ":\n  Normalized (continuous variables) and OneHotEncoded (categorical variables)\n\nR^2 score for multi-linear LASSO regression against SE010:\n  " + str(round(r2_score_lasso_reg_all, 4)) + " (4 d.p.)\n")

    coef_labels = set_of_independent_evaluated_variables
    coef_selected_output_list = []
    for i in range(0,len(coef_labels)):
        if model_lasso_all.coef_[i] != 0.0: # NOT EQUALS ZERO
            coef_selected_output_list += [[round(model_lasso_all.coef_[i],4), coef_labels[i]]]
        print(str(round(model_lasso_all.coef_[i],4)) + "(" + coef_labels[i] + ")")
    return coef_selected_output_list



def FADN_lasso_selected(filename):
    
    shortlisted_variables = FADN_lasso_all(filename) #call previous 'all variables' lasso regression function
    shortlisted_variable_identifiers = []
    for i in range(0,len(shortlisted_variables)):
        shortlisted_variable_identifiers.append(str(shortlisted_variables[i][1]))
    
    fadn_df = fadn_df_rep[['SE010'] + shortlisted_variable_identifiers] #'SYS02' is 'farms represented' #understand python will use 'fadn_df_rep' from global retrieval here, without further keyword calls
    
    # subsequent code written with guidance from URL: https://www.kaggle.com/thaddeussegura/enough-to-be-dangerous-multiple-linear-regression

    Y = fadn_df.iloc[:,0] #.values for np array
    X = fadn_df.iloc[:,1:] #.values for np array
    X_scaled_1 = StandardScaler().fit_transform(X[shortlisted_variable_identifiers])

    # Guided by tutorial available at URL: https://machinelearningmastery.com/lasso-regression-with-python/
    model_lasso_1 = Lasso(alpha=1.0)
    model_lasso_1.fit(X_scaled_1, Y)
    r2_score_lasso_reg_1 = model_lasso_1.score(X_scaled_1, Y)
    print("\nLASSO REGRESSION FUNCTION - Selected shortlisted variables from " + filename + ":\n\n" + str(shortlisted_variable_identifiers) + "\n  Normalized (continuous variables)\n\nR^2 score for multi-linear LASSO regression against SE010:\n  " + str(round(r2_score_lasso_reg_1, 4)) + " (4 d.p.)\n")

    coef_labels = shortlisted_variable_identifiers
    for i in range(0,len(coef_labels)):
        print(str(round(model_lasso_1.coef_[i], 4)) + "(" + coef_labels[i] + ")")
    print("\n\n\n\n")
    
    return shortlisted_variable_identifiers


# RUNNING OLS REGRESSION MODEL (statsmodels) (previously for comparison to MLEM with 'REGION' as random effect, now as reset of coefficients for predicting values from features selected after scaling during Lasso regression)
# https://www.statsmodels.org/devel/regression.html

def run_ols(filename):
    
    # read data using Lasso-selected CS-specific feature-selected variables, plus dependent variable
    # guided by tutorial available at URL: https://www.datarobot.com/blog/multiple-regression-using-statsmodels/
    data_cs = fadn_df_rep[national_variables_selected_dict[filename] + ['SE010']]
    X = data_cs[national_variables_selected_dict[filename]]
    y = data_cs['SE010']
    X = sm.add_constant(X)
    # fit the OLS model and print output
    ols_est = sm.OLS(y, X).fit()
    
    return [ols_est.summary(), ols_est.aic]


#NUTS3 data for Catalonia: https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units/nuts
#NUTS3 map for Spain: https://ec.europa.eu/eurostat/documents/345175/7451602/2021-NUTS-3-map-ES.pdf


# ES511 CODE

# REGIONAL FADN FILTERING FUNCTION

def df_fadn_regional_filtering(filename, fadn_df):
    
    # FADN regressions: FADN region and NUTS3 specific and 2017 specific
    # FADN regions map: https://webgate.ec.europa.eu/ricaprod/private/images/othermaps/fadnrica_eu28_2013_a4.pdf
    
    if filename == "DEU2017.csv": # e.g. Mulde, German specific
        fadn_df = fadn_df.loc[fadn_df['REGION'] == 114] # Mulde's majority FADN region
        fadn_df = fadn_df.loc[fadn_df['NUTS3'].isin(['DED4', 'DED5', 'DED41', 'DED42', 'DED43', 'DED44', 'DED45', 'DED52', 'DED53'])]
    #elif filename... for other countries and their respective FADN regions
    elif filename == "UKI2017.csv":
        fadn_df = fadn_df.loc[fadn_df['REGION'].isin([411, 412])] # code i.e. if == 411 or == 412 FADN region
        fadn_df = fadn_df.loc[fadn_df['NUTS3'].isin(['UKE1', 'UKE2', 'UKE3', 'UKE4', 'UKF1', 'UKF3', 'UKE12', 'UKE13', 'UKE21', 'UKE22', 'UKE31', 'UKE45', 'UKF15', 'UKF30'])]
    elif filename == "CZE2017.csv":
        fadn_df = fadn_df.loc[fadn_df['REGION'] == 745]
        fadn_df = fadn_df.loc[fadn_df['NUTS3'].isin(['CZ06', 'CZ07', 'CZ064', 'CZ072'])]
    elif filename == "ESP2017.csv":
        fadn_df = fadn_df.loc[fadn_df['REGION'] == 535]
        fadn_df = fadn_df.loc[fadn_df['NUTS3'] == "ES511"] #breaking up Catalonia into NUTS regions for analysis
    else:
        raise ValueError("Unrecognised FADN filename for BESTMAP EU case study regions.")
    
    return fadn_df

# RUN REGRESSION WORKFLOW

national_variables_selected_dict = {}

national_ols_model_summary_output_dict = {}
national_ols_model_aic_output_dict = {}

for filename in ["ESP2017.csv"]:
    
    #ZERO/NaN FADN COLUMN EXCLUSIONS (in the case of crop non-presence in given agricultural region)
    print("\n STEP 1", filename, "Zero/NaN column stage:") #for readability purposes
    continuous_variables_list = ['SE005','SE025','SE030','SE035','SE041','SE042','SE046','SE050','SE054','SE055','SE065','SE071','SE073','SE074','SE075','SYS02']
    continuous_variables_list_filtered = df_column_filtering_fadn(filename, continuous_variables_list)
    
    #MULTICOLLINEARITY EXCLUSIONS
    print("\n STEP 2", filename, "Multicollinearity stage:") #for readability purposes
    fadn_df_rep = weighted_df_for_correlations_generator(filename)
    set_of_independent_evaluated_variables = looped_optimisation_collinearity(fadn_df_rep, continuous_variables_list_filtered)  + ['AGE','TF8','SEX','REGION','ALTITUDE','ANC']
    print("Set of independent variables to take forward, post-collinearity exclusions:", set_of_independent_evaluated_variables) #readability addition / variable exclusion diagnosis
    
    # RUN FEATURE-SELECTING LASSO MODEL FOR SELECTED COUNTRIES
    print("\n STEP 3", filename, "Lasso stage:") #for readability purposes
    national_values = FADN_lasso_selected(filename) #run feature-selecting Lasso model, then run again with just the non-zero coefficient variables...
    national_variables_selected_dict[filename] = national_values
    
    # RUN LINEAR MODEL FOR ALL COUNTRIES
    national_ols_model_output = run_ols(filename) #run ols model, using feature-selected variables from previous Lasso model (no 'grouping' here, obviously)
    national_ols_model_summary_output_dict[filename[0:7] + "_OLS"] = national_ols_model_output[0] #save ols summaries into retreviable dictionary
    national_ols_model_aic_output_dict[filename[0:7] + "_OLS"] = national_ols_model_output[1] #save ols aic results into retreviable dictionary
    

print(national_ols_model_summary_output_dict) #visual error checking

# CODE FOR USE WITH REAL DATA

# per farm, from imported 'IACS/LPIS' microdata

shapefile_name = "catalan_lpis/filtered_files/lpis_catalan_nuts3_es511.shp"
food_and_fodder_output_file_name = "foodfodder_farm_2017_es.txt"

# import S.O. 'csv' file
so_values_table = pd.read_table(food_and_fodder_output_file_name, sep=" ")

farm_lpis_iacs_gdf = gpd.read_file(shapefile_name) # import LPIS/IACS microdata

#year filtering
farm_lpis_iacs_gdf = farm_lpis_iacs_gdf.loc[farm_lpis_iacs_gdf['CAMPANYA'] == 2017]

# calculating field/farm area from GIS properties
# step 1: create field area using '.geometry.area' properties
farm_lpis_iacs_gdf = farm_lpis_iacs_gdf.to_crs(epsg=6933) # equal area projection suitable for geopandas area operations #https://stackoverflow.com/questions/69217181/how-can-i-convert-geopandas-crs-units-to-meters2
farm_lpis_iacs_gdf['field_area'] = farm_lpis_iacs_gdf.geometry.area # units of area linked to CRS geoseries units
farm_lpis_iacs_gdf['field_area_ha'] = farm_lpis_iacs_gdf['field_area'] / 10000 #converting m2 to ha for field area units

saving_string = 'es511'
saving_title = 'Province of Barcelona'


# ES512 CODE

# REGIONAL FADN FILTERING FUNCTION

def df_fadn_regional_filtering(filename, fadn_df):
    
    # FADN regressions: FADN region and NUTS3 specific and 2017 specific
    # FADN regions map: https://webgate.ec.europa.eu/ricaprod/private/images/othermaps/fadnrica_eu28_2013_a4.pdf
    
    if filename == "DEU2017.csv": # e.g. Mulde, German specific
        fadn_df = fadn_df.loc[fadn_df['REGION'] == 114] # Mulde's majority FADN region
        fadn_df = fadn_df.loc[fadn_df['NUTS3'].isin(['DED4', 'DED5', 'DED41', 'DED42', 'DED43', 'DED44', 'DED45', 'DED52', 'DED53'])]
    #elif filename... for other countries and their respective FADN regions
    elif filename == "UKI2017.csv":
        fadn_df = fadn_df.loc[fadn_df['REGION'].isin([411, 412])] # code i.e. if == 411 or == 412 FADN region
        fadn_df = fadn_df.loc[fadn_df['NUTS3'].isin(['UKE1', 'UKE2', 'UKE3', 'UKE4', 'UKF1', 'UKF3', 'UKE12', 'UKE13', 'UKE21', 'UKE22', 'UKE31', 'UKE45', 'UKF15', 'UKF30'])]
    elif filename == "CZE2017.csv":
        fadn_df = fadn_df.loc[fadn_df['REGION'] == 745]
        fadn_df = fadn_df.loc[fadn_df['NUTS3'].isin(['CZ06', 'CZ07', 'CZ064', 'CZ072'])]
    elif filename == "ESP2017.csv":
        fadn_df = fadn_df.loc[fadn_df['REGION'] == 535]
        fadn_df = fadn_df.loc[fadn_df['NUTS3'] == "ES512"] #breaking up Catalonia into NUTS regions for analysis
    else:
        raise ValueError("Unrecognised FADN filename for BESTMAP EU case study regions.")
    
    return fadn_df

# RUN REGRESSION WORKFLOW

national_variables_selected_dict = {}

national_ols_model_summary_output_dict = {}
national_ols_model_aic_output_dict = {}

for filename in ["ESP2017.csv"]:
    
    #ZERO/NaN FADN COLUMN EXCLUSIONS (in the case of crop non-presence in given agricultural region)
    print("\n STEP 1", filename, "Zero/NaN column stage:") #for readability purposes
    continuous_variables_list = ['SE005','SE025','SE030','SE035','SE041','SE042','SE046','SE050','SE054','SE055','SE065','SE071','SE073','SE074','SE075','SYS02']
    continuous_variables_list_filtered = df_column_filtering_fadn(filename, continuous_variables_list)
    
    #MULTICOLLINEARITY EXCLUSIONS
    print("\n STEP 2", filename, "Multicollinearity stage:") #for readability purposes
    fadn_df_rep = weighted_df_for_correlations_generator(filename)
    set_of_independent_evaluated_variables = looped_optimisation_collinearity(fadn_df_rep, continuous_variables_list_filtered)  + ['AGE','TF8','SEX','REGION','ALTITUDE','ANC']
    print("Set of independent variables to take forward, post-collinearity exclusions:", set_of_independent_evaluated_variables) #readability addition / variable exclusion diagnosis
    
    # RUN FEATURE-SELECTING LASSO MODEL FOR SELECTED COUNTRIES
    print("\n STEP 3", filename, "Lasso stage:") #for readability purposes
    national_values = FADN_lasso_selected(filename) #run feature-selecting Lasso model, then run again with just the non-zero coefficient variables...
    national_variables_selected_dict[filename] = national_values
    
    # RUN LINEAR MODEL FOR ALL COUNTRIES
    national_ols_model_output = run_ols(filename) #run ols model, using feature-selected variables from previous Lasso model (no 'grouping' here, obviously)
    national_ols_model_summary_output_dict[filename[0:7] + "_OLS"] = national_ols_model_output[0] #save ols summaries into retreviable dictionary
    national_ols_model_aic_output_dict[filename[0:7] + "_OLS"] = national_ols_model_output[1] #save ols aic results into retreviable dictionary
    

print(national_ols_model_summary_output_dict) #visual error checking

# CODE FOR USE WITH REAL DATA

# per farm, from imported 'IACS/LPIS' microdata

shapefile_name = "catalan_lpis/filtered_files/lpis_catalan_nuts3_es512.shp"
food_and_fodder_output_file_name = "foodfodder_farm_2017_es.txt"

# import S.O. 'csv' file
so_values_table = pd.read_table(food_and_fodder_output_file_name, sep=" ")

farm_lpis_iacs_gdf = gpd.read_file(shapefile_name) # import LPIS/IACS microdata

#year filtering
farm_lpis_iacs_gdf = farm_lpis_iacs_gdf.loc[farm_lpis_iacs_gdf['CAMPANYA'] == 2017]

# calculating field/farm area from GIS properties
# step 1: create field area using '.geometry.area' properties
farm_lpis_iacs_gdf = farm_lpis_iacs_gdf.to_crs(epsg=6933) # equal area projection suitable for geopandas area operations #https://stackoverflow.com/questions/69217181/how-can-i-convert-geopandas-crs-units-to-meters2
farm_lpis_iacs_gdf['field_area'] = farm_lpis_iacs_gdf.geometry.area # units of area linked to CRS geoseries units
farm_lpis_iacs_gdf['field_area_ha'] = farm_lpis_iacs_gdf['field_area'] / 10000 #converting m2 to ha for field area units

saving_string = 'es512'
saving_title = 'Province of Girona'


# ES513 CODE

# REGIONAL FADN FILTERING FUNCTION

def df_fadn_regional_filtering(filename, fadn_df):
    
    # FADN regressions: FADN region and NUTS3 specific and 2017 specific
    # FADN regions map: https://webgate.ec.europa.eu/ricaprod/private/images/othermaps/fadnrica_eu28_2013_a4.pdf
    
    if filename == "DEU2017.csv": # e.g. Mulde, German specific
        fadn_df = fadn_df.loc[fadn_df['REGION'] == 114] # Mulde's majority FADN region
        fadn_df = fadn_df.loc[fadn_df['NUTS3'].isin(['DED4', 'DED5', 'DED41', 'DED42', 'DED43', 'DED44', 'DED45', 'DED52', 'DED53'])]
    #elif filename... for other countries and their respective FADN regions
    elif filename == "UKI2017.csv":
        fadn_df = fadn_df.loc[fadn_df['REGION'].isin([411, 412])] # code i.e. if == 411 or == 412 FADN region
        fadn_df = fadn_df.loc[fadn_df['NUTS3'].isin(['UKE1', 'UKE2', 'UKE3', 'UKE4', 'UKF1', 'UKF3', 'UKE12', 'UKE13', 'UKE21', 'UKE22', 'UKE31', 'UKE45', 'UKF15', 'UKF30'])]
    elif filename == "CZE2017.csv":
        fadn_df = fadn_df.loc[fadn_df['REGION'] == 745]
        fadn_df = fadn_df.loc[fadn_df['NUTS3'].isin(['CZ06', 'CZ07', 'CZ064', 'CZ072'])]
    elif filename == "ESP2017.csv":
        fadn_df = fadn_df.loc[fadn_df['REGION'] == 535]
        fadn_df = fadn_df.loc[fadn_df['NUTS3'] == "ES513"] #breaking up Catalonia into NUTS regions for analysis
    else:
        raise ValueError("Unrecognised FADN filename for BESTMAP EU case study regions.")
    
    return fadn_df

# RUN REGRESSION WORKFLOW

national_variables_selected_dict = {}

national_ols_model_summary_output_dict = {}
national_ols_model_aic_output_dict = {}

for filename in ["ESP2017.csv"]:
    
    #ZERO/NaN FADN COLUMN EXCLUSIONS (in the case of crop non-presence in given agricultural region)
    print("\n STEP 1", filename, "Zero/NaN column stage:") #for readability purposes
    continuous_variables_list = ['SE005','SE025','SE030','SE035','SE041','SE042','SE046','SE050','SE054','SE055','SE065','SE071','SE073','SE074','SE075','SYS02']
    continuous_variables_list_filtered = df_column_filtering_fadn(filename, continuous_variables_list)
    
    #MULTICOLLINEARITY EXCLUSIONS
    print("\n STEP 2", filename, "Multicollinearity stage:") #for readability purposes
    fadn_df_rep = weighted_df_for_correlations_generator(filename)
    set_of_independent_evaluated_variables = looped_optimisation_collinearity(fadn_df_rep, continuous_variables_list_filtered)  + ['AGE','TF8','SEX','REGION','ALTITUDE','ANC']
    print("Set of independent variables to take forward, post-collinearity exclusions:", set_of_independent_evaluated_variables) #readability addition / variable exclusion diagnosis
    
    # RUN FEATURE-SELECTING LASSO MODEL FOR SELECTED COUNTRIES
    print("\n STEP 3", filename, "Lasso stage:") #for readability purposes
    national_values = FADN_lasso_selected(filename) #run feature-selecting Lasso model, then run again with just the non-zero coefficient variables...
    national_variables_selected_dict[filename] = national_values
    
    # RUN LINEAR MODEL FOR ALL COUNTRIES
    national_ols_model_output = run_ols(filename) #run ols model, using feature-selected variables from previous Lasso model (no 'grouping' here, obviously)
    national_ols_model_summary_output_dict[filename[0:7] + "_OLS"] = national_ols_model_output[0] #save ols summaries into retreviable dictionary
    national_ols_model_aic_output_dict[filename[0:7] + "_OLS"] = national_ols_model_output[1] #save ols aic results into retreviable dictionary
    

print(national_ols_model_summary_output_dict) #visual error checking

# CODE FOR USE WITH REAL DATA

# per farm, from imported 'IACS/LPIS' microdata

shapefile_name = "catalan_lpis/filtered_files/lpis_catalan_nuts3_es513.shp"
food_and_fodder_output_file_name = "foodfodder_farm_2017_es.txt"

# import S.O. 'csv' file
so_values_table = pd.read_table(food_and_fodder_output_file_name, sep=" ")

farm_lpis_iacs_gdf = gpd.read_file(shapefile_name) # import LPIS/IACS microdata

#year filtering
farm_lpis_iacs_gdf = farm_lpis_iacs_gdf.loc[farm_lpis_iacs_gdf['CAMPANYA'] == 2017]

# calculating field/farm area from GIS properties
# step 1: create field area using '.geometry.area' properties
farm_lpis_iacs_gdf = farm_lpis_iacs_gdf.to_crs(epsg=6933) # equal area projection suitable for geopandas area operations #https://stackoverflow.com/questions/69217181/how-can-i-convert-geopandas-crs-units-to-meters2
farm_lpis_iacs_gdf['field_area'] = farm_lpis_iacs_gdf.geometry.area # units of area linked to CRS geoseries units
farm_lpis_iacs_gdf['field_area_ha'] = farm_lpis_iacs_gdf['field_area'] / 10000 #converting m2 to ha for field area units

saving_string = 'es513'
saving_title = 'Province of Lleida'


# ES514 CODE

# REGIONAL FADN FILTERING FUNCTION

def df_fadn_regional_filtering(filename, fadn_df):
    
    # FADN regressions: FADN region and NUTS3 specific and 2017 specific
    # FADN regions map: https://webgate.ec.europa.eu/ricaprod/private/images/othermaps/fadnrica_eu28_2013_a4.pdf
    
    if filename == "DEU2017.csv": # e.g. Mulde, German specific
        fadn_df = fadn_df.loc[fadn_df['REGION'] == 114] # Mulde's majority FADN region
        fadn_df = fadn_df.loc[fadn_df['NUTS3'].isin(['DED4', 'DED5', 'DED41', 'DED42', 'DED43', 'DED44', 'DED45', 'DED52', 'DED53'])]
    #elif filename... for other countries and their respective FADN regions
    elif filename == "UKI2017.csv":
        fadn_df = fadn_df.loc[fadn_df['REGION'].isin([411, 412])] # code i.e. if == 411 or == 412 FADN region
        fadn_df = fadn_df.loc[fadn_df['NUTS3'].isin(['UKE1', 'UKE2', 'UKE3', 'UKE4', 'UKF1', 'UKF3', 'UKE12', 'UKE13', 'UKE21', 'UKE22', 'UKE31', 'UKE45', 'UKF15', 'UKF30'])]
    elif filename == "CZE2017.csv":
        fadn_df = fadn_df.loc[fadn_df['REGION'] == 745]
        fadn_df = fadn_df.loc[fadn_df['NUTS3'].isin(['CZ06', 'CZ07', 'CZ064', 'CZ072'])]
    elif filename == "ESP2017.csv":
        fadn_df = fadn_df.loc[fadn_df['REGION'] == 535]
        fadn_df = fadn_df.loc[fadn_df['NUTS3'] == "ES514"] #breaking up Catalonia into NUTS regions for analysis
    else:
        raise ValueError("Unrecognised FADN filename for BESTMAP EU case study regions.")
    
    return fadn_df

# RUN REGRESSION WORKFLOW

national_variables_selected_dict = {}

national_ols_model_summary_output_dict = {}
national_ols_model_aic_output_dict = {}

for filename in ["ESP2017.csv"]:
    
    #ZERO/NaN FADN COLUMN EXCLUSIONS (in the case of crop non-presence in given agricultural region)
    print("\n STEP 1", filename, "Zero/NaN column stage:") #for readability purposes
    continuous_variables_list = ['SE005','SE025','SE030','SE035','SE041','SE042','SE046','SE050','SE054','SE055','SE065','SE071','SE073','SE074','SE075','SYS02']
    continuous_variables_list_filtered = df_column_filtering_fadn(filename, continuous_variables_list)
    
    #MULTICOLLINEARITY EXCLUSIONS
    print("\n STEP 2", filename, "Multicollinearity stage:") #for readability purposes
    fadn_df_rep = weighted_df_for_correlations_generator(filename)
    set_of_independent_evaluated_variables = looped_optimisation_collinearity(fadn_df_rep, continuous_variables_list_filtered)  + ['AGE','TF8','SEX','REGION','ALTITUDE','ANC']
    print("Set of independent variables to take forward, post-collinearity exclusions:", set_of_independent_evaluated_variables) #readability addition / variable exclusion diagnosis
    
    # RUN FEATURE-SELECTING LASSO MODEL FOR SELECTED COUNTRIES
    print("\n STEP 3", filename, "Lasso stage:") #for readability purposes
    national_values = FADN_lasso_selected(filename) #run feature-selecting Lasso model, then run again with just the non-zero coefficient variables...
    national_variables_selected_dict[filename] = national_values
    
    # RUN LINEAR MODEL FOR ALL COUNTRIES
    national_ols_model_output = run_ols(filename) #run ols model, using feature-selected variables from previous Lasso model (no 'grouping' here, obviously)
    national_ols_model_summary_output_dict[filename[0:7] + "_OLS"] = national_ols_model_output[0] #save ols summaries into retreviable dictionary
    national_ols_model_aic_output_dict[filename[0:7] + "_OLS"] = national_ols_model_output[1] #save ols aic results into retreviable dictionary
    

print(national_ols_model_summary_output_dict) #visual error checking

# CODE FOR USE WITH REAL DATA

# per farm, from imported 'IACS/LPIS' microdata

shapefile_name = "catalan_lpis/filtered_files/lpis_catalan_nuts3_es514.shp"
food_and_fodder_output_file_name = "foodfodder_farm_2017_es.txt"

# import S.O. 'csv' file
so_values_table = pd.read_table(food_and_fodder_output_file_name, sep=" ")

farm_lpis_iacs_gdf = gpd.read_file(shapefile_name) # import LPIS/IACS microdata

#year filtering
farm_lpis_iacs_gdf = farm_lpis_iacs_gdf.loc[farm_lpis_iacs_gdf['CAMPANYA'] == 2017]

# calculating field/farm area from GIS properties
# step 1: create field area using '.geometry.area' properties
farm_lpis_iacs_gdf = farm_lpis_iacs_gdf.to_crs(epsg=6933) # equal area projection suitable for geopandas area operations #https://stackoverflow.com/questions/69217181/how-can-i-convert-geopandas-crs-units-to-meters2
farm_lpis_iacs_gdf['field_area'] = farm_lpis_iacs_gdf.geometry.area # units of area linked to CRS geoseries units
farm_lpis_iacs_gdf['field_area_ha'] = farm_lpis_iacs_gdf['field_area'] / 10000 #converting m2 to ha for field area units

saving_string = 'es514'
saving_title = 'Province of Tarragona'


# Universal End Code

# data pre-transformed into .CSV format from .MDB format
catalonia_dun_2017_datafile = pd.read_csv("catalan_lpis/BESTMAP_V4_2017_v2.csv", encoding = 'latin-1')

merged_table_cat_dun_lpis = pd.merge(left = farm_lpis_iacs_gdf, right = catalonia_dun_2017_datafile, how = 'inner', left_on = 'ID_REC', right_on = 'ID_REC')

# for the Catalonia model, farm_id 'ID_EXPLOACIO' will be stored as a string data type (no casting needed for this - natural form)
income_model_output_df_extended_no_subsidies = pd.DataFrame(columns = ['ID_EXPLOTACIO', 'fnva_value_of_production_aes_non_adoption', 'fnva_value_of_production_aes_adoption']) # generate output df to start definining columns - no subsidies
income_model_output_df_extended = pd.DataFrame(columns = ['ID_EXPLOTACIO', 'fnva_value_of_production_aes_non_adoption', 'fnva_value_of_production_aes_adoption']) # generate output df to start definining columns - subsidies

for farm_id in list(merged_table_cat_dun_lpis['ID_EXPLOTACIO'].unique()):
    
    # retrieve df row containing S.O. values from food and fodder model output - more efficient to retrieve once here
    try: #error checking built-in

        so_row = so_values_table.loc[so_values_table['ID_EXPLOTACIO'] == farm_id] #may need string/int casting conversions here for this comparison of unique farm_ids

        row_to_add = {'ID_EXPLOTACIO': farm_id, 'fnva_value_of_production_aes_non_adoption': (float(so_row['so'])), 'fnva_value_of_production_aes_adoption': (float(so_row['so_mod']))} #dict to add as row in df 'income_model_output_df_extended'

        # NO SUBSIDIES
        income_model_output_df_extended_no_subsidies = income_model_output_df_extended_no_subsidies.append(row_to_add, ignore_index=True)
        # SUBSIDIES
        income_model_output_df_extended = income_model_output_df_extended.append(row_to_add, ignore_index=True)
                
    except TypeError: #at this stage, any farm_ids not available in standard output data will be removed from the dataset, according to built-in error prevention/checking measures
        print("TypeError: farm_id 'ID_EXPLOTACIO' " + str(farm_id) + " skipped (likely a mismatched farm with no Standard Output data from Food and Fodder output available)...")
        continue


# subsidy guidance here provided by expert contributions to BESTMAP:

# ESP Flower Strips: N/A (€0.00)
# ESP Catch/Cover Crops ('AES_367'): PER CROP: VINEYARDS = €145.00, FRUITS = €162.81, NUTS = €206.67, OTHER = €65.96...  / ha per annum; FIXED VALUE €95.92 / ha per annum
# ESP Maintaining Grassland ('AES_363', 'AES_368'): €280.80 / ha per annum (€522.00, €522.00, €120.00, €120.00, €120.00 across 5 yr scheme) for AES_363; €30.00 / ha per annum for AES_368
# ESP Conversion of Arable Land to Grassland: N/A (€0.00)

farms_to_include_list = [] #monitors AES adopters (only to proceed with farms that satisfy recorded AES adoption)

for farm_id in list(income_model_output_df_extended['ID_EXPLOTACIO']):

    farm_fields = merged_table_cat_dun_lpis.loc[merged_table_cat_dun_lpis['ID_EXPLOTACIO'] == farm_id]
    total_aes_value_to_add = 0
    
    for field_i in range(0,len(farm_fields)): # for all the fields associated with the farm in the 'IACS/LPIS' microdata
        field_row = farm_fields.iloc[field_i,]
        
        #AES_367 'Integrated production' by fixed overall mean average constant
        #if field_row['AES_367'] == 1: #cover crops scheme - 'Integrated production'
        #    total_aes_value_to_add += field_row['field_area_ha'] * 95.92 #fixed constant
        
        #AES_367 'Integrated production' by general crop type categories
        if field_row['AES_367'] == 1: #cover crops scheme - 'Integrated production'
            if field_row['US'] in ['VI']: #if clearly 'Vineyard' land use...
                total_aes_value_to_add += field_row['field_area_ha'] * 145.00 #varying constant by crop category: mean of crop category payment rates
            elif field_row['US'] in ['FY','CI','TH','OV','OF','VF']: #if clearly 'Fruits' land use...
                total_aes_value_to_add += field_row['field_area_ha'] * 162.81 #varying constant by crop category: mean of crop category payment rates
            elif field_row['US'] in ['FS','FV','FL']: #if clearly 'Nuts' land use...
                total_aes_value_to_add += field_row['field_area_ha'] * 206.67 #varying constant by crop category: mean of crop category payment rates
            else: #if 'Other' land use (e.g. general arable crops)...
                total_aes_value_to_add += field_row['field_area_ha'] * 65.96 #varying constant by crop category: mean of crop category payment rates
        
        if field_row['AES_363'] == 1: #grassland maintenance scheme - 'Management and recovery of meadows and pastures'
            total_aes_value_to_add += field_row['field_area_ha'] * 280.80 #fixed constant
        
        if field_row['AES_368'] == 1: #grassland maintenance scheme - 'Conservation of native breeds'
            total_aes_value_to_add += field_row['field_area_ha'] * 30.00 #fixed constant
        
        #add further schemes on this indentation here, as judged reasonable against the modelling schematic
        
    if total_aes_value_to_add > 0: #monitors AES adopters (only to proceed with farms that satisfy recorded AES adoption)
        farms_to_include_list += [farm_id] #monitors AES adopters (only to proceed with farms that satisfy recorded AES adoption)
    
    # SUBSIDIES
    updated_value_to_assign = float(income_model_output_df_extended.loc[income_model_output_df_extended['ID_EXPLOTACIO'] == farm_id]['fnva_value_of_production_aes_adoption']) + total_aes_value_to_add
    income_model_output_df_extended.iloc[income_model_output_df_extended.loc[income_model_output_df_extended['ID_EXPLOTACIO'] == farm_id].index[0],2] = updated_value_to_assign #column 2 as this is always AES adoption value column


income_model_output_df_extended = income_model_output_df_extended.loc[income_model_output_df_extended['ID_EXPLOTACIO'].isin(farms_to_include_list)].reset_index()
income_model_output_df_extended_no_subsidies = income_model_output_df_extended_no_subsidies.loc[income_model_output_df_extended_no_subsidies['ID_EXPLOTACIO'].isin(farms_to_include_list)].reset_index()


# Guy Ziv's method: which 'IACS/LPIS' variables resemble the values feature-selected from FADN regressoin of AWU...

def area_crop_names_in_ha(farm_id, crop_names):
    
    total_crop_area_for_farm = []
    for name in crop_names:
        fields_concerned = merged_table_cat_dun_lpis.loc[(merged_table_cat_dun_lpis['ID_EXPLOTACIO'] == farm_id) & (merged_table_cat_dun_lpis['US'] == name)].reset_index() #int/string issues here potentially
        for fields_i in range(0,len(fields_concerned)):
            total_crop_area_for_farm += [float(fields_concerned['field_area_ha'][fields_i])]
        
    total_crop_area_for_farm_summed = sum(total_crop_area_for_farm)
    return total_crop_area_for_farm_summed #in ha units



# FADN to 'IACS/LPIS' data conversion functions

def fetch_econ_size(farm_id, adoption_determination_string, subsidies_determination_string): #SE005
    
    if adoption_determination_string == "non-adoption": # econ size is dynamic as it itself depends on the AES adoption scenario...
        if subsidies_determination_string == "no subsidies":
            econ_size_estimate = float(income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_non_adoption'].loc[income_model_output_df_extended_no_subsidies['ID_EXPLOTACIO'] == farm_id]) / 1000.0 # divide by 1000 for units
        else:
            econ_size_estimate = float(income_model_output_df_extended['fnva_value_of_production_aes_non_adoption'].loc[income_model_output_df_extended['ID_EXPLOTACIO'] == farm_id]) / 1000.0 # divide by 1000 for units
    
    elif adoption_determination_string == "adoption":
        if subsidies_determination_string == "no subsidies":
            econ_size_estimate = float(income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_adoption'].loc[income_model_output_df_extended_no_subsidies['ID_EXPLOTACIO'] == farm_id]) / 1000.0 # divide by 1000 for units        
        else:
            econ_size_estimate = float(income_model_output_df_extended['fnva_value_of_production_aes_adoption'].loc[income_model_output_df_extended['ID_EXPLOTACIO'] == farm_id]) / 1000.0 # divide by 1000 for units
    
    else:
        print("Error in fetching economic size using 'IACS/LPIS': unrecognised 'adoption_determination_string' scenario")
        return None
    
    return econ_size_estimate

def fetch_total_uaa(farm_id): #SE025
    total_uaa_estimate = merged_table_cat_dun_lpis.loc[merged_table_cat_dun_lpis['ID_EXPLOTACIO'] == farm_id]['field_area_ha'].sum()
    return total_uaa_estimate

def fetch_rented_uaa(farm_id): #SE030 #assume 50% of farm area is rented for estimate here - should be more accurate on aggregate
    rented_uaa_estimate = 0.5 * merged_table_cat_dun_lpis.loc[merged_table_cat_dun_lpis['ID_EXPLOTACIO'] == farm_id]['field_area_ha'].sum()
    return rented_uaa_estimate

def fetch_cereals_area(farm_id): #SE035
    cereals_crop_type_names = ['TA']
    cereals_area_estimate = area_crop_names_in_ha(farm_id, cereals_crop_type_names)
    return cereals_area_estimate

def fetch_other_field_crops(farm_id): #SE041
    other_field_crops_estimate = 0 #0 as not readily in Catalan 'IACS/LPIS'
    return other_field_crops_estimate

def fetch_energy_crops(farm_id): #SE042
    energy_crops_estimate = 0 #0 as not readily in Catalan 'IACS/LPIS'
    return energy_crops_estimate

def fetch_vegetables_and_flowers(farm_id): #SE046
    vegetables_and_flowers_crop_type_names = ['FY', 'CI', 'OF']
    vegetables_and_flowers_estimate = area_crop_names_in_ha(farm_id, vegetables_and_flowers_crop_type_names)
    return vegetables_and_flowers_estimate

def fetch_vineyards(farm_id): #SE050
    vinyards_crop_type_names = ['VI', 'VF', 'FV']
    vineyards_estimate = area_crop_names_in_ha(farm_id, vinyards_crop_type_names)
    return vineyards_estimate

def fetch_permanent_crops(farm_id): #SE054
    permanent_crops_crop_type_names = ['FY', 'CI', 'FS', 'OV', 'VI', 'TH', 'OF', 'VF', 'FV', 'FL']
    permanent_crops_estimate = area_crop_names_in_ha(farm_id, permanent_crops_crop_type_names)
    return permanent_crops_estimate

def fetch_orchards(farm_id): #SE055
    orchards_crop_type_names = ['TH']
    orchards_estimate = area_crop_names_in_ha(farm_id, orchards_crop_type_names)
    return orchards_estimate

def fetch_olive_groves(farm_id): #SE060
    olive_groves_crop_type_names = ['OV', 'FL', 'OF']
    olive_groves_estimate = area_crop_names_in_ha(farm_id, olive_groves_crop_type_names)
    return olive_groves_estimate

def fetch_other_permanent_crops(farm_id): #SE065
    other_permanent_crops_crop_type_names = ['IV']
    other_permanent_crops_estimate = area_crop_names_in_ha(farm_id, other_permanent_crops_crop_type_names) #judged to be a minimal influence, put greenhouse here as could accurately be understood to resemble special/specialist growing conditions
    return other_permanent_crops_estimate

def fetch_forage_crops(farm_id): #SE071
    forage_crops_crop_type_names = ['PA', 'PR', 'PS']
    forage_crops_estimate = area_crop_names_in_ha(farm_id, forage_crops_crop_type_names)
    return forage_crops_estimate

def fetch_set_aside(farm_id): #SE073
    set_aside_estimate = 0 #0 as not readily in 'IACS/LPIS'
    return set_aside_estimate

def fetch_total_ag_area_out_of_production(farm_id): #SE074
    ag_out_of_production_crop_type_names = ['ZV', 'AG', 'ZU', 'ED', 'IM']
    total_ag_area_out_of_production_estimate = area_crop_names_in_ha(farm_id, ag_out_of_production_crop_type_names)
    return total_ag_area_out_of_production_estimate

def fetch_woodland_area(farm_id): #SE075
    woodland_area_crop_type_names = ['FO', 'PA']
    woodland_area_estimate = area_crop_names_in_ha(farm_id, woodland_area_crop_type_names)
    return woodland_area_estimate

def fetch_farm_manager_age(farm_id): #AGE
    farm_manager_age_estimate = 0 #0 as not readily in 'IACS/LPIS'
    return farm_manager_age_estimate

def fetch_tf8(farm_id): #TF8
    econ_size_estimate = 0 #0 as not readily in 'IACS/LPIS'
    return econ_size_estimate

def fetch_farm_manager_sex(farm_id): #SEX
    farm_manager_sex_estimate = 0 #0 as not readily in 'IACS/LPIS'
    return farm_manager_sex_estimate

def fetch_region(farm_id): #REGION
    region_estimate = 0 #0 as not readily in 'IACS/LPIS'
    return region_estimate

def fetch_altitude(farm_id): #ALTITUDE
    altitude_estimate = 0 #0 as not readily in 'IACS/LPIS'
    return altitude_estimate

def fetch_anc(farm_id): #ANC
    anc_estimate = 0 #0 as not readily in 'IACS/LPIS'
    return anc_estimate


# Guy Ziv's method: which 'IACS/LPIS' variables resemble the values feature-selected from FADN regressoin of AWU...

import copy

# first, automatically retrieve the linear regression formula from the output dict from earlier, in non-disclosive manner

def retrieve_coef_table(filename):    
    ols_output_object = national_ols_model_summary_output_dict[filename[0:7] + "_OLS"]
    coef_table = []
    for i in range(1,len(ols_output_object.tables[1])): #begins at 1 to uninclude column headings, which are not needed
        coef_table += [[ols_output_object.tables[1].data[i][0],float(ols_output_object.tables[1].data[i][1].strip())]] #produces table of term (string) + coefficient (float) from statsmodels OLS regression output
    return coef_table


def generate_substituted_coef_table(coef_table, farm_id):
    
    global adoption_determination_string
    global subsidies_determination_string
    
    coef_table_substituted = coef_table
    # so this function must take an FADN variable from the OLS regression (for all other than SE005, which needs different parameters), then retrieve a method NAME for how to calculate it for 'IACS/LPIS' using a given 'farm_id'
    # step 1: lookup method...
    fadn_values_in_lpis_iacs_variables_translation_dict = {'SE025': fetch_total_uaa,
                                                           'SE030': fetch_rented_uaa,
                                                           'SE035': fetch_cereals_area,
                                                           'SE041': fetch_other_field_crops,
                                                           'SE042': fetch_energy_crops,
                                                           'SE046': fetch_vegetables_and_flowers,
                                                           'SE050': fetch_vineyards,
                                                           'SE054': fetch_permanent_crops,
                                                           'SE055': fetch_orchards,
                                                           'SE060': fetch_olive_groves,
                                                           'SE065': fetch_other_permanent_crops,
                                                           'SE071': fetch_forage_crops,
                                                           'SE073': fetch_set_aside,
                                                           'SE074': fetch_total_ag_area_out_of_production,
                                                           'SE075': fetch_woodland_area,
                                                           'AGE': fetch_farm_manager_age,
                                                           'TF8': fetch_tf8,
                                                           'SEX': fetch_farm_manager_sex,
                                                           'REGION': fetch_region,
                                                           'ALTITUDE': fetch_altitude,
                                                           'ANC': fetch_anc} # with these functions (whose references are) stored here, started with 0 value, which will wipe out the component if returned, and build improvement where practical
    
    for i in range(1,len(coef_table_substituted)):
        if coef_table_substituted[i][0] == 'SE005': #bring SE005 down here, as has different parameters so can't work within universal generalised call when 'decalling the dictionary function values' as proposed here: https://betterprogramming.pub/too-many-if-elif-conditions-in-python-use-dictionaries-instead-5486299af27e 
            coef_table_substituted[i][0] = fetch_econ_size(farm_id, adoption_determination_string, subsidies_determination_string)
        # step 2: within method (called outside dictionary with farm_id parameter) calculate value... (unless was request for value for SE005)
        else:
            coef_table_substituted[i][0] = fadn_values_in_lpis_iacs_variables_translation_dict[coef_table_substituted[i][0]](farm_id) #generate_fadn_variable_value_using_lpis_iacs[coef_table_substituted[i][0]](farm_id) which retrieves the function from the dictionary then calls the values
    
    return coef_table_substituted


def calculate_awu_estimate(coef_table_substituted):
    
    final_formula_awu_components_with_values = list()
    final_formula_awu_components_with_values += [coef_table_substituted[0][1]] #'coef_table_substituted[0][1]' is (the first and only) constant term, at index 0, thus...
    
    for i in range(1,len(coef_table_substituted)): #... begins at 1 here as begins the indexing in helpful way for the indexing of the nested list during the loop
        component_value_to_add = (coef_table_substituted[i][0] * coef_table_substituted[i][1])
        final_formula_awu_components_with_values += [component_value_to_add] #remember, adding list values here, as totals for each component in estimated reg. formula for AWU

    awu_estimate = sum(final_formula_awu_components_with_values)
    return awu_estimate




coef_table_to_input = retrieve_coef_table("ESP2017.csv") # this regression formula same for all farms in given CS region/subregion studied in a run of the programme...

awu_estimates_list_aes_non_adoption = []
awu_estimates_list_aes_adoption = []
awu_estimates_list_aes_non_adoption_no_subsidies = []
awu_estimates_list_aes_adoption_no_subsidies = []

farm_id_list = [] # error/assumption checking

print("Total number of farms to process:", int(len(list(income_model_output_df_extended['ID_EXPLOTACIO'])))) #visual performance reporting

for farm_id in list(income_model_output_df_extended['ID_EXPLOTACIO']):
    
    # HIST 1
    subsidies_determination_string = "subsidies"
    adoption_determination_string = "non-adoption"
    sub_coef_table_for_farm_aes_non_adoption = generate_substituted_coef_table(copy.deepcopy(coef_table_to_input), farm_id) #applying formula # 'deep copy' of list variable needed here to use as constant and prevent changes in each iteration to the 'formula' embedded list - copy() or [:] weren't working sufficiently (as 'shallow copies'?)
    awu_estimate_for_farm_aes_non_adoption = calculate_awu_estimate(sub_coef_table_for_farm_aes_non_adoption) #applying formula
    awu_estimates_list_aes_non_adoption += [awu_estimate_for_farm_aes_non_adoption] #add estimate for AWU labour for particular farm_id for **AES non-adoption** to become list to become column in 'income_model_output_df_extended'
    
    adoption_determination_string = "adoption"
    sub_coef_table_for_farm_aes_adoption = generate_substituted_coef_table(copy.deepcopy(coef_table_to_input), farm_id) #applying formula # 'deep copy' of list variable needed here to use as constant and prevent changes in each iteration to the 'formula' embedded list - copy() or [:] weren't working sufficiently (as 'shallow copies'?)
    awu_estimate_for_farm_aes_adoption = calculate_awu_estimate(sub_coef_table_for_farm_aes_adoption) #applying formula
    awu_estimates_list_aes_adoption += [awu_estimate_for_farm_aes_adoption] #add estimate for AWU labour for particular farm_id for **AES adoption** to become list to become column in 'income_model_output_df_extended'
    
    # HIST 2
    subsidies_determination_string = "no subsidies"
    adoption_determination_string = "non-adoption"
    sub_coef_table_for_farm_aes_non_adoption_no_subsidies = generate_substituted_coef_table(copy.deepcopy(coef_table_to_input), farm_id) #applying formula # 'deep copy' of list variable needed here to use as constant and prevent changes in each iteration to the 'formula' embedded list - copy() or [:] weren't working sufficiently (as 'shallow copies'?)
    awu_estimate_for_farm_aes_non_adoption_no_subsidies = calculate_awu_estimate(sub_coef_table_for_farm_aes_non_adoption_no_subsidies) #applying formula
    awu_estimates_list_aes_non_adoption_no_subsidies += [awu_estimate_for_farm_aes_non_adoption_no_subsidies] #add estimate for AWU labour for particular farm_id for **AES non-adoption** to become list to become column in 'income_model_output_df_extended'
    
    adoption_determination_string = "adoption"
    sub_coef_table_for_farm_aes_adoption_no_subsidies = generate_substituted_coef_table(copy.deepcopy(coef_table_to_input), farm_id) #applying formula # 'deep copy' of list variable needed here to use as constant and prevent changes in each iteration to the 'formula' embedded list - copy() or [:] weren't working sufficiently (as 'shallow copies'?)
    awu_estimate_for_farm_aes_adoption_no_subsidies = calculate_awu_estimate(sub_coef_table_for_farm_aes_adoption_no_subsidies) #applying formula
    awu_estimates_list_aes_adoption_no_subsidies += [awu_estimate_for_farm_aes_adoption_no_subsidies] #add estimate for AWU labour for particular farm_id for **AES adoption** to become list to become column in 'income_model_output_df_extended'
    
    farm_id_list += [farm_id] # for purposes of error checking assumptions on ordered lists etc.
    
    if len(farm_id_list) % 100 == 0: #print every hundred done (i.e. dividing number done by 100 has remainder 0, as determined using modulus function) #visual performance reporting
        print("Farms now complete = " + str(len(farm_id_list))) #visual performance reporting


# Then, reflect the standardisation by AWU in the FNVA estimate by here dividing the total FNVA revenue estimate by the estimate for AWU (adoption or non-adoption estimate) per farm in both dataframes (for adoption/non-adoption scenarios)

income_model_output_df_extended_no_subsidies['AWU_estimate_AES_non_adoption'] = awu_estimates_list_aes_non_adoption_no_subsidies #assumes farm_id location / ordered lists in Python
income_model_output_df_extended_no_subsidies['AWU_estimate_AES_adoption'] = awu_estimates_list_aes_adoption_no_subsidies #assumes farm_id location / ordered lists in Python
income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate'] = income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_non_adoption'] / income_model_output_df_extended_no_subsidies['AWU_estimate_AES_non_adoption']
income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate'] = income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_adoption'] / income_model_output_df_extended_no_subsidies['AWU_estimate_AES_adoption']

income_model_output_df_extended['AWU_estimate_AES_non_adoption'] = awu_estimates_list_aes_non_adoption #assumes farm_id location / ordered lists in Python
income_model_output_df_extended['AWU_estimate_AES_adoption'] = awu_estimates_list_aes_adoption #assumes farm_id location / ordered lists in Python
income_model_output_df_extended['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate'] = income_model_output_df_extended['fnva_value_of_production_aes_non_adoption'] / income_model_output_df_extended['AWU_estimate_AES_non_adoption']
income_model_output_df_extended['fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate'] = income_model_output_df_extended['fnva_value_of_production_aes_adoption'] / income_model_output_df_extended['AWU_estimate_AES_adoption']


# get rid of zeros, re-save results
income_model_output_df_extended_no_subsidies_no_zeros = income_model_output_df_extended_no_subsidies.loc[(income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate'] > 0.0) & (income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate'] > 0.0)].reset_index()
income_model_output_df_extended_no_zeros = income_model_output_df_extended.loc[(income_model_output_df_extended['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate'] > 0.0) & (income_model_output_df_extended['fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate'] > 0.0)].reset_index()


## ENDING 1: D4.2 EXTRACTION: Create percent change, pair down columns, save as csv file

income_model_output_df_extended_no_zeros['percent_change_to_AES_adoption_4dp'] = round(((income_model_output_df_extended_no_zeros['fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate'] - income_model_output_df_extended_no_zeros['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate'])/income_model_output_df_extended_no_zeros['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate']) * 100,4) #rounded 4.d.p.
income_model_output_df_extended_no_zeros_concise = income_model_output_df_extended_no_zeros[['ID_EXPLOTACIO', 'percent_change_to_AES_adoption_4dp']]
income_model_output_df_extended_no_zeros_concise.to_csv("final_results_es_tradeoff/" + saving_string + "/" + saving_string + "_data_fnva_income_change_table_catalonia_spain_with_subsidies_no_zeros.csv")

#combine individual provinces / NUTS3 regions into single table for all farm income percent changes available for all of Catalonia CS
catalan_table_1 = pd.read_csv("final_results_es_tradeoff/es511/es511_data_fnva_income_change_table_catalonia_spain_with_subsidies_no_zeros.csv")
catalan_table_2 = pd.read_csv("final_results_es_tradeoff/es512/es512_data_fnva_income_change_table_catalonia_spain_with_subsidies_no_zeros.csv")
catalan_table_3 = pd.read_csv("final_results_es_tradeoff/es513/es513_data_fnva_income_change_table_catalonia_spain_with_subsidies_no_zeros.csv")
#catalan_table_4 = pd.read_csv("final_results_es_tradeoff/es514/es514_data_fnva_income_change_table_catalonia_spain_with_subsidies_no_zeros.csv")
catalan_table_unified = catalan_table_1.append(catalan_table_2).append(catalan_table_3) #.append(catalan_table_4)
catalan_table_unified.reset_index()[['ID_EXPLOTACIO', 'percent_change_to_AES_adoption_4dp']].to_csv("final_results_es_tradeoff/combined_table_all_available_figures_fnva_income_change_catalonia_spain_with_subsidies_no_zero_euros.csv")


## ENDING 2: Original Code

# Combine back into new single dataframe, with two/three columns: 'farm_region' (optional, CS region and/or region within e.g. Catalonia etc.), 'FNVA_AES_adoption', 'FNVA_no_AES_adoption'
final_results_df_fnva_estimates_no_subsidies = pd.DataFrame(columns = ['farm_region', 'FNVA_no_AES_adoption', 'FNVA_AES_adoption'])
final_results_df_fnva_estimates_no_subsidies['FNVA_AES_adoption'] = income_model_output_df_extended_no_subsidies_no_zeros['fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate']
final_results_df_fnva_estimates_no_subsidies['FNVA_no_AES_adoption'] = income_model_output_df_extended_no_subsidies_no_zeros['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate']
final_results_df_fnva_estimates_no_subsidies['farm_region'] = 'Catalonia, ESP'

final_results_df_fnva_estimates_no_subsidies['percent_change_to_AES_adoption_4dp'] = round(((final_results_df_fnva_estimates_no_subsidies['FNVA_AES_adoption'] - final_results_df_fnva_estimates_no_subsidies['FNVA_no_AES_adoption'])/final_results_df_fnva_estimates_no_subsidies['FNVA_no_AES_adoption']) * 100,4) #rounded 4.d.p.
#final_results_df_fnva_estimates_no_subsidies.describe().to_csv("final_results_es/" + saving_string + "/" + saving_string + "_results_summary_statistics_describe_table_catalonia_spain_no_subsidies_no_zeros_final.csv")


# Combine back into new single dataframe, with two/three columns: 'farm_region' (optional, CS region and/or region within e.g. Catalonia etc.), 'FNVA_AES_adoption', 'FNVA_no_AES_adoption'
final_results_df_fnva_estimates = pd.DataFrame(columns = ['farm_region', 'FNVA_no_AES_adoption', 'FNVA_AES_adoption'])
final_results_df_fnva_estimates['FNVA_AES_adoption'] = income_model_output_df_extended_no_zeros['fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate']
final_results_df_fnva_estimates['FNVA_no_AES_adoption'] = income_model_output_df_extended_no_zeros['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate']
final_results_df_fnva_estimates['farm_region'] = 'Catalonia, ESP'

final_results_df_fnva_estimates['percent_change_to_AES_adoption_4dp'] = round(((final_results_df_fnva_estimates['FNVA_AES_adoption'] - final_results_df_fnva_estimates['FNVA_no_AES_adoption'])/final_results_df_fnva_estimates['FNVA_no_AES_adoption']) * 100,4) #rounded 4.d.p.
final_results_df_fnva_estimates.describe().to_csv("final_results_es/" + saving_string + "/" + saving_string + "_results_summary_statistics_describe_table_catalonia_spain_with_subsidies_no_zeros_final.csv")


# BOXPLOTS
plt.figure(figsize=(8,8))
n_value = len(final_results_df_fnva_estimates)
final_results_df_fnva_estimates.boxplot(column=['FNVA_no_AES_adoption','FNVA_AES_adoption'], grid=False, color='b', showfliers=False); #https://stackoverflow.com/questions/22028064/matplotlib-boxplot-without-outliers
plt.ylabel("Value of Agricultural Production [Gross Income / Total Revenue, part of FNVA] (€)\n")
plt.xlabel("\nData Acknowledgements: FADN data in methodology for ESP (2017) from Directorate-General for Agriculture and Rural Development,\n European Commission. 'IACS/LPIS' data (2017) is DUN data from the Department for Agriculture of the Government of Catalonia.", size = 8)
plt.xticks([1,2],["No AES adoption\nscenario", "AES adoption\nscenario"])
plt.title("Boxplots of distributions for estimated 'Value of Agricultural Production' values (€)\n for Agri-Environment Scheme ('AES') non-adoption and adoption scenarios. \n" + saving_title + ", Catalonia, Spain (n="+str(n_value)+") [Data: FADN (2017),'IACS/LPIS' (2017)].\n[€0 income farms removed, outliers not displayed]\n", size = 12);
plt.savefig("final_results_es/" + saving_string + "/boxplot_" + saving_string + "_catalonia_farm_income_model_output_no_zeros_no_outliers_final.png", bbox_inches='tight')


import seaborn as sns
plt.rcParams['figure.dpi'] = 150 #https://blakeaw.github.io/2020-05-25-improve-matplotlib-notebook-inline-res/
plt.rcParams['savefig.dpi'] = 150 #https://blakeaw.github.io/2020-05-25-improve-matplotlib-notebook-inline-res/


# HISTOGRAM: PERCENT CHANGE FULL DISTRIBUTION FIGURE

n_value = len(final_results_df_fnva_estimates)

min_number_farms_per_bin_total_to_2dp = 6

plt.figure(figsize=(10.0,5.0))
sns.displot(data=final_results_df_fnva_estimates['percent_change_to_AES_adoption_4dp'], stat='probability', bins=(int((n_value/min_number_farms_per_bin_total_to_2dp)-1)), color = 'black');
from matplotlib.ticker import PercentFormatter
plt.gca().yaxis.set_major_formatter(PercentFormatter(1))
plt.gca().xaxis.set_major_formatter(PercentFormatter(100))
plt.xlim(int((final_results_df_fnva_estimates['percent_change_to_AES_adoption_4dp'].min()) - 2), int((final_results_df_fnva_estimates['percent_change_to_AES_adoption_4dp'].max()) + 2)) #SDC protecting automatic
plt.ylim(0,) #percentage of sample
plt.xlabel("\nEstimated percent difference in FNVA from adopting AES (%),\n" + str(round(1/min_number_farms_per_bin_total_to_2dp,2)) + " >= total bins per farm in sample (to 2 d.p.)\n\n\nData Acknowledgements: FADN data in methodology for ESP (2017) from Directorate-General\n for Agriculture and Rural Development, European Commission. 'IACS/LPIS' data (2017)\n is DUN data from the Department for Agriculture of the Government of Catalonia.")
plt.ylabel("Percent of farm sample (%)\n")
plt.title("Proportion of farm sample (n = " + str(n_value) + ") exhibiting (approximate) estimate percent \ndifference in Farm Net Value Added ('FNVA', C.27 PMEF) from adopting AES.\n" + saving_title + ", Catalonia, Spain, 2017 [Data: FADN, 'IACS/LPIS'].\n[€0 income farms removed]\n", fontsize= 12)
plt.savefig("final_results_es/" + saving_string + "/" + saving_string + "_histogram_es_farm_income_model_output_no_zeros_final.png", bbox_inches ='tight')