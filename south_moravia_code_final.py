# Tested Environment: Python version 3.7.11, Geopandas version 0.9.0

# GENERAL IMPORTS

import geopandas as gpd
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# MODEL IMPORTS

from sklearn.linear_model import Lasso # linear and multi-linear regression model fitting
from sklearn.preprocessing import OneHotEncoder, StandardScaler # OneHotEncoding, Normalization / Feature Scaling

import statsmodels.api as sm
import statsmodels.formula.api as smf


# REGIONAL FADN FILTERING FUNCTION

def df_fadn_regional_filtering(filename, fadn_df):
    
    # FADN regressions: FADN region and NUTS3 specific and 2017 specific
    # FADN regions map: https://webgate.ec.europa.eu/ricaprod/private/images/othermaps/fadnrica_eu28_2013_a4.pdf
    
    if filename == "DEU2017.csv": # e.g. Mulde, German specific
        fadn_df = fadn_df.loc[fadn_df['REGION'] == 114] # Mulde's majority FADN region
        fadn_df = fadn_df.loc[fadn_df['NUTS3'].isin(['DED4', 'DED5', 'DED41', 'DED42', 'DED43', 'DED44', 'DED45', 'DED52', 'DED53'])]
    #elif filename... for other countries and their respective FADN regions
    elif filename == "UKI2017.csv":
        fadn_df = fadn_df.loc[fadn_df['REGION'].isin([411, 412])] # code i.e. if == 411 or == 412 FADN region
        fadn_df = fadn_df.loc[fadn_df['NUTS3'].isin(['UKE1', 'UKE2', 'UKE3', 'UKE4', 'UKF1', 'UKF3', 'UKE12', 'UKE13', 'UKE21', 'UKE22', 'UKE31', 'UKE45', 'UKF15', 'UKF30'])]
    elif filename == "CZE2017.csv":
        fadn_df = fadn_df.loc[fadn_df['REGION'] == 745]
        fadn_df = fadn_df.loc[fadn_df['NUTS3'].isin(['CZ06', 'CZ07', 'CZ064', 'CZ072'])]
    elif filename == "ESP2017.csv":
        fadn_df = fadn_df.loc[fadn_df['REGION'] == 535]
    else:
        raise ValueError("Unrecognised FADN filename for BESTMAP EU case study regions.")
    
    return fadn_df


# COLUMN FILTERING FUNCTION (preventing crashes)

def df_column_filtering_fadn(filename, fadn_variables_list):
    
    empty_columns_list = []
    country_df_fadn = pd.read_csv(filename)
    country_df_fadn = df_fadn_regional_filtering(filename, country_df_fadn)

    for variable in fadn_variables_list:
        if (country_df_fadn[variable].min() == 0.0) & (country_df_fadn[variable].max() == 0.0): #if column only contains 0 float values...
            empty_columns_list += [variable] # ... eliminate the column from the remainder of the analysis
        elif len(country_df_fadn[variable]) == 0: #need to filter out empty columns with no 0 values but all 'nan's etc... a column of all nans has length = 0 so harnessing this property
            empty_columns_list += [variable] # ... eliminate the column from the remainder of the analysis
    
    print("Empty/nan variable columns in FADN region data, to be removed from analysis:",empty_columns_list) #output readability
    
    fadn_variables_list_filtered = [x for x in fadn_variables_list if x not in empty_columns_list] #generate new list of columns
    
    return fadn_variables_list_filtered


# WEIGHTING FUNCTION

def df_transform_to_weighted_farms_rep(input_df): # function to transform dataframe into new dataframe with 1 duplicate row for each 'farms represented' from each FADN row
    
    old_df = input_df.copy().reset_index()
    old_df['SYS02'] = [round(x,0) for x in old_df['SYS02']] #round to nearest integer, up or down
    new_df = pd.DataFrame(columns=old_df.columns) #declares in right df shape with columns from old df

    row_i = 0
    for row in range(0,len(old_df)): #for each row in original dataframe
        row_values_list = list(old_df.iloc[row,]) #efficiency
        for i in range(0,int(old_df['SYS02'][row])): #for each time a farm is represented for that row # int casting causing forced 'rounding down' e.g. 3.8 => 3 (with exceptions for float values appearing to represent recurring digital points).
            row_i += 1
            new_df.loc[row_i] = row_values_list #assign a duplicate row into the new dataframe with the same values

    return new_df


# CODE TO DETERMINE MULTICOLLINEARITY EXCLUSIONS AUTOMATICALLY

# excludes at least one variable if the absolute correlation coefficient (r value) exceeds 0.8 between two variables according to corr() function in pandas
# excludes the variable with lower direct R^2 value to the dependant variable - here 'SE010' labour

def return_unilateral_ols_score(variable, fadn_df_rep):
    trial_feature = str(variable)
    data_cs = fadn_df_rep[[trial_feature] + ['SE010']]
    X = data_cs[trial_feature]
    y = data_cs['SE010']
    X = sm.add_constant(X)
    ols_est = sm.OLS(y, X).fit()
    return ols_est.rsquared #https://stackoverflow.com/questions/48522609/how-to-retrieve-model-estimates-from-statsmodels


def looped_optimisation_collinearity(fadn_df_rep, set_of_original_variables):    
    #LOOPED FROM HERE ON IN UNTIL NO CONFLICTS REMAIN (can save more variables to stay in the model than just eliminating all conflicting variables at once at first)
    i = 0
    while True:
        
        should_go_list = []
        
        i += 1
        if i == 1: #first iteration
            set_of_original_variables.remove('SYS02') #removes ['SYS02'] ahead of correlations evaluations - see https://www.w3schools.com/python/python_lists_remove.asp
        
        fadn_df = fadn_df_rep[set_of_original_variables] #only continuous will appear in correlation matrix and thus be filtered out using this process
        fadn_df_correlations = fadn_df.copy().corr() #generates correlation matrix df
        
        # comb through correlation results to find pairs to evaluate
        list_of_variable_pairings_to_evaluate = []
        for variable in set_of_original_variables:
            if len(fadn_df_correlations.loc[abs(fadn_df_correlations[variable]) > 0.8 ]) > 1:
                variables_list_to_inspect = list(fadn_df_correlations.loc[abs(fadn_df_correlations[variable]) > 0.8 ].index.values)
                list_of_variable_pairings_to_evaluate += [variables_list_to_inspect]
        
        unique_pairings = [list(x) for x in set(tuple(x) for x in list_of_variable_pairings_to_evaluate)] #https://stackoverflow.com/questions/3724551/python-uniqueness-for-list-of-lists

        for nested_list in unique_pairings:
            if len(nested_list) > 1:
                variable_ols_r2_scores_dict = {}
                for variable in nested_list:
                    variable_ols_r2_scores_dict[variable] = return_unilateral_ols_score(variable, fadn_df_rep)
                should_go_list += [x for x in nested_list if variable_ols_r2_scores_dict[x] != max(variable_ols_r2_scores_dict.values())] #all but the highest R2 variable go
        
        if len(should_go_list) == 0:
            break
            
        should_go_list = list(set(should_go_list)) #make unique values only to stop repeat entries
        
        set_of_original_variables = [x for x in set_of_original_variables if x not in should_go_list]
        

    set_of_independent_evaluated_variables = set_of_original_variables
    return set_of_independent_evaluated_variables


def weighted_df_for_correlations_generator(filename):
    
    fadn_df = pd.read_csv(filename) #read in data
    
    #regional filtering
    fadn_df = df_fadn_regional_filtering(filename, fadn_df)
    
    # deploy weighting function to consider 'SYS02' value in regressions
    fadn_df_rep = df_transform_to_weighted_farms_rep(fadn_df).reset_index()

    return fadn_df_rep


# LASSO FUNCTIONS

def FADN_lasso_all(filename):
    
    global fadn_df_rep #to avoid repeat calculation after calculating again here
    
    fadn_df = pd.read_csv(filename) #read in data
    fadn_df = df_fadn_regional_filtering(filename, fadn_df)
    
    fadn_df_rep = df_transform_to_weighted_farms_rep(fadn_df).reset_index() # deploy weighting function to consider 'SYS02' value in regressions
    
    all_variables = ['SE010'] + set_of_independent_evaluated_variables + ['SYS02']
    
    fadn_df = fadn_df_rep[all_variables] #'SYS02' is 'farms represented'

    # subsequent code written with guidance from URL: https://www.kaggle.com/thaddeussegura/enough-to-be-dangerous-multiple-linear-regression
    X = fadn_df.iloc[:,1:-1] #.values for np array
    Y = fadn_df.iloc[:,0] #.values for np array

    # https://stackoverflow.com/questions/43798377/one-hot-encode-categorical-variables-and-scale-continuous-ones-simultaneouely
    columns_to_be_scaled = [x for x in set_of_independent_evaluated_variables if x not in ['AGE','TF8','SEX','REGION','ALTITUDE','ANC']] #continuous
    columns_to_be_encoded = ['AGE','TF8','SEX','REGION','ALTITUDE','ANC'] #categorical

    scaled_columns = StandardScaler().fit_transform(X[columns_to_be_scaled])
    encoded_columns = OneHotEncoder(sparse=False).fit_transform(X[columns_to_be_encoded])

    X_scaled_encoded = np.concatenate([scaled_columns, encoded_columns], axis = 1)
    
    # Guided by tutorial available at URL: https://machinelearningmastery.com/lasso-regression-with-python/
    model_lasso_all = Lasso(alpha=1.0)
    model_lasso_all.fit(X_scaled_encoded, Y)

    r2_score_lasso_reg_all = model_lasso_all.score(X_scaled_encoded, Y)
    print("\nLASSO REGRESSION FUNCTION - All longlisted variables from " + filename + ":\n  Normalized (continuous variables) and OneHotEncoded (categorical variables)\n\nR^2 score for multi-linear LASSO regression against SE010:\n  " + str(round(r2_score_lasso_reg_all, 4)) + " (4 d.p.)\n")

    coef_labels = set_of_independent_evaluated_variables
    coef_selected_output_list = []
    for i in range(0,len(coef_labels)):
        if model_lasso_all.coef_[i] != 0.0: # NOT EQUALS ZERO
            coef_selected_output_list += [[round(model_lasso_all.coef_[i],4), coef_labels[i]]]
        print(str(round(model_lasso_all.coef_[i],4)) + "(" + coef_labels[i] + ")")
    return coef_selected_output_list



def FADN_lasso_selected(filename):
    
    shortlisted_variables = FADN_lasso_all(filename) #call previous 'all variables' lasso regression function
    shortlisted_variable_identifiers = []
    for i in range(0,len(shortlisted_variables)):
        shortlisted_variable_identifiers.append(str(shortlisted_variables[i][1]))
    
    fadn_df = fadn_df_rep[['SE010'] + shortlisted_variable_identifiers] #'SYS02' is 'farms represented' #understand python will use 'fadn_df_rep' from global retrieval here, without further keyword calls
    
    # subsequent code written with guidance from URL: https://www.kaggle.com/thaddeussegura/enough-to-be-dangerous-multiple-linear-regression

    Y = fadn_df.iloc[:,0] #.values for np array
    X = fadn_df.iloc[:,1:] #.values for np array
    X_scaled_1 = StandardScaler().fit_transform(X[shortlisted_variable_identifiers])

    # Guided by tutorial available at URL: https://machinelearningmastery.com/lasso-regression-with-python/
    model_lasso_1 = Lasso(alpha=1.0)
    model_lasso_1.fit(X_scaled_1, Y)
    r2_score_lasso_reg_1 = model_lasso_1.score(X_scaled_1, Y)
    print("\nLASSO REGRESSION FUNCTION - Selected shortlisted variables from " + filename + ":\n\n" + str(shortlisted_variable_identifiers) + "\n  Normalized (continuous variables)\n\nR^2 score for multi-linear LASSO regression against SE010:\n  " + str(round(r2_score_lasso_reg_1, 4)) + " (4 d.p.)\n")

    coef_labels = shortlisted_variable_identifiers
    for i in range(0,len(coef_labels)):
        print(str(round(model_lasso_1.coef_[i], 4)) + "(" + coef_labels[i] + ")")
    print("\n\n\n\n")
    
    return shortlisted_variable_identifiers


# RUNNING OLS REGRESSION MODEL (statsmodels) (previously for comparison to MLEM with 'REGION' as random effect, now as reset of coefficients for predicting values from features selected after scaling during Lasso regression)
# https://www.statsmodels.org/devel/regression.html

import statsmodels.api as sm 
import statsmodels.formula.api as smf

def run_ols(filename):
    
    # read data using Lasso-selected CS-specific feature-selected variables, plus dependent variable
    # guided by tutorial available at URL: https://www.datarobot.com/blog/multiple-regression-using-statsmodels/
    data_cs = fadn_df_rep[national_variables_selected_dict[filename] + ['SE010']]
    X = data_cs[national_variables_selected_dict[filename]]
    y = data_cs['SE010']
    X = sm.add_constant(X)
    # fit the OLS model and print output
    ols_est = sm.OLS(y, X).fit()
    
    return [ols_est.summary(), ols_est.aic]


national_variables_selected_dict = {}

national_ols_model_summary_output_dict = {}
national_ols_model_aic_output_dict = {}

for filename in ["CZE2017.csv"]: #for all countries... ["CZE2017.csv","UKI2017.csv","DEU2017.csv","ESP2017.csv"]
    
    #ZERO/NaN FADN COLUMN EXCLUSIONS (in the case of crop non-presence in given agricultural region)
    print("\n STEP 1", filename, "Zero/NaN column stage:") #for readability purposes
    continuous_variables_list = ['SE005','SE025','SE030','SE035','SE041','SE042','SE046','SE050','SE054','SE055','SE065','SE071','SE073','SE074','SE075','SYS02']
    continuous_variables_list_filtered = df_column_filtering_fadn(filename, continuous_variables_list)
    
    #MULTICOLLINEARITY EXCLUSIONS
    print("\n STEP 2", filename, "Multicollinearity stage:") #for readability purposes
    fadn_df_rep = weighted_df_for_correlations_generator(filename)
    set_of_independent_evaluated_variables = looped_optimisation_collinearity(fadn_df_rep, continuous_variables_list_filtered)  + ['AGE','TF8','SEX','REGION','ALTITUDE','ANC']
    print("Set of independent variables to take forward, post-collinearity exclusions:", set_of_independent_evaluated_variables) #readability addition / variable exclusion diagnosis
    
    # RUN FEATURE-SELECTING LASSO MODEL FOR SELECTED COUNTRIES
    print("\n STEP 3", filename, "Lasso stage:") #for readability purposes
    national_values = FADN_lasso_selected(filename) #run feature-selecting Lasso model, then run again with just the non-zero coefficient variables...
    national_variables_selected_dict[filename] = national_values
    
    # RUN LINEAR MODEL FOR ALL COUNTRIES
    national_ols_model_output = run_ols(filename) #run ols model, using feature-selected variables from previous Lasso model (no 'grouping' here, obviously)
    national_ols_model_summary_output_dict[filename[0:7] + "_OLS"] = national_ols_model_output[0] #save ols summaries into retreviable dictionary
    national_ols_model_aic_output_dict[filename[0:7] + "_OLS"] = national_ols_model_output[1] #save ols aic results into retreviable dictionary

print(national_ols_model_summary_output_dict) #visual error checking


# CODE FOR USE WITH REAL DATA

# per farm, from imported 'IACS/LPIS' microdata

shapefile_name = "czech_lpis/cz lpis data/cz lpis data/DPB_merge_2017_ETRS_clip.shp"
food_and_fodder_output_file_name = "foodfodder_model_inputs_outputs_2017_cz/2017_foodfodder_InputAndOutput_cz_20221021/2017_foodfodder_model_output_cz.txt" # coded for .txt format for this 'csv' output

# import S.O. 'csv' file (food and fodder model output)
so_values_table = pd.read_table(food_and_fodder_output_file_name, sep='\t', engine='python') #https://stackoverflow.com/questions/27896214/reading-tab-delimited-file-with-pandas-works-on-windows-but-not-on-mac

# import crop/AES 'csv' file (food and fodder model input)
# specific to Czech CS region: how we can determine crop presense and AES application at the field-level in English. Pre-prepared by BESTMAP colleagues.
food_and_fodder_input_file_name = "foodfodder_model_inputs_outputs_2017_cz/2017_foodfodder_InputAndOutput_cz_20221021/2017_foodfodder_input_cz.txt" # coded for .txt format for this 'csv' output
crop_and_aes_values_table = pd.read_table(food_and_fodder_input_file_name, encoding='latin-1') #https://stackoverflow.com/questions/19699367/for-line-in-results-in-unicodedecodeerror-utf-8-codec-cant-decode-byte

# calculate total value for area per S.O. crop (and per AES application in case of scheme-specific-modified S.O. coefficient) => already built into food_fodder modelling workflow

income_model_output_df_extended = pd.DataFrame(columns = ['ID_UZ', 'fnva_value_of_production_aes_non_adoption', 'fnva_value_of_production_aes_adoption']) # generate output df to start definining columns
farm_lpis_iacs_gdf = gpd.read_file(shapefile_name) # import 'IACS/LPIS' microdata

# calculating field/farm area from GIS properties
# step 1: create field area using '.geometry.area' properties
farm_lpis_iacs_gdf = farm_lpis_iacs_gdf.to_crs(epsg=6933) # equal area projection suitable for geopandas area operations #https://stackoverflow.com/questions/69217181/how-can-i-convert-geopandas-crs-units-to-meters2
farm_lpis_iacs_gdf['field_area'] = farm_lpis_iacs_gdf.geometry.area # units of area linked to CRS geoseries units
farm_lpis_iacs_gdf['field_area_ha'] = farm_lpis_iacs_gdf['field_area'] / 10000 #converting m2 to ha for field area units


# NO SUBSIDIES LATER ADDED EDITION
income_model_output_df_extended_no_subsidies = pd.DataFrame(columns = ['ID_UZ', 'fnva_value_of_production_aes_non_adoption', 'fnva_value_of_production_aes_adoption']) # generate output df to start definining columns

for farm_id in list(farm_lpis_iacs_gdf['ID_UZ'].unique()):
    
    # retrieve df row containing S.O. coefficients from food and fodder model output - more efficient to retrieve once here
    try: #error checking built-in

        so_row = so_values_table.loc[so_values_table['ID_UZ'] == farm_id] #may need string/int casting conversions here for this comparison of unique farm_ids
        
        row_to_add = {'ID_UZ': int(farm_id), 'fnva_value_of_production_aes_non_adoption': (float(so_row['so'])), 'fnva_value_of_production_aes_adoption': (float(so_row['so_mod']))} #dict to add as row in df 'income_model_output_df_extended'

        # NO SUBSIDIES LATER ADDED
        income_model_output_df_extended_no_subsidies = income_model_output_df_extended.append(row_to_add, ignore_index=True)        
        # NORMAL (WITH SUBSIDIES LATER ADDED)
        income_model_output_df_extended = income_model_output_df_extended.append(row_to_add, ignore_index=True)

    except TypeError: #at this stage, any farm_ids not available in standard output data will be removed from the dataset, according to built-in error prevention/checking measures
        print("TypeError: farm_id 'ID_UZ' " + str(farm_id) + " skipped (likely a mismatched farm with no Standard Output data from Food and Fodder output available)...")
        continue

income_model_output_df_extended_no_subsidies['ID_UZ'] = income_model_output_df_extended_no_subsidies['ID_UZ'].astype(int) #correcting back to int from float (due to processing, originally int dtype)
income_model_output_df_extended['ID_UZ'] = income_model_output_df_extended['ID_UZ'].astype(int) #correcting back to int from float (due to processing, originally int dtype)


print(income_model_output_df_extended_no_subsidies) #visual error checking
print(income_model_output_df_extended) #visual error checking


# subsidy guidance here provided by expert contributions to BESTMAP:

# CZE Flower Strips (): €591.00 / ha per annum
# CZE Catch/Cover Crops (): N/A
# CZE Maintaining Grassland (): €180.00 / ha per annum
# CZE Conversion of Arable Land to Grassland: €400.00 / ha per annum

cze_aes_type_translation_dict = {"BufferAreas": "flower_strips", "catch_cover_crops": "catch_cover_crops", "MaintainigGrasslands": "maintaining_grassland","LUConvGrassland": "conversion_arable_to_grassland"}
cze_aes_payment_values_dict = {"flower_strips": 591.00, "catch_cover_crops": 0.00, "maintaining_grassland": 180.00, "conversion_arable_to_grassland": 400.00}

farms_to_include_list = [] #monitors AES adopters (only to proceed with farms that satisfy recorded AES adoption)

# in adoption value table...
for farm_id in list(income_model_output_df_extended['ID_UZ']): #may not need to cast into list for purposes of for loop iteration
    
    # if known that *specific* AES being applied, add specific scheme income to each farm's total income estimate (as the component for 'Pillar II payments' income for C.27 FNVA indicator)
    
    farm_fields = crop_and_aes_values_table.loc[crop_and_aes_values_table['ID_UZ'] == farm_id]
    total_aes_value_to_add = 0
    
    for field_i in range(0,len(farm_fields)): # for all the fields associated with the farm in the 'IACS/LPIS' microdata
        field_row = farm_fields.iloc[field_i,]
        
        if str(field_row['AES_group']) in list(cze_aes_type_translation_dict.keys()):
            aes_value_to_add_per_ha_per_year = cze_aes_payment_values_dict[cze_aes_type_translation_dict[str(field_row['AES_group'])]]
            aes_value_to_add_applied_to_ha = aes_value_to_add_per_ha_per_year * field_row['AES_area'] #usually do 'field_area_ha' as multiplication scalar column
            total_aes_value_to_add += aes_value_to_add_applied_to_ha #add to running total to add from all fields AES subsidy incomes
    
    if total_aes_value_to_add > 0: #monitors AES adopters (only to proceed with farms that satisfy recorded AES adoption)
        farms_to_include_list += [farm_id] #monitors AES adopters (only to proceed with farms that satisfy recorded AES adoption)
    
    # now add AES total ('total_aes_value_to_add') to farm-level AES adoption income estimation ('fnva_value_of_production_aes_adoption' column value) in extended output table ('income_model_output_df_extended')
    
    updated_value_to_assign = float(income_model_output_df_extended.loc[income_model_output_df_extended['ID_UZ'] == farm_id]['fnva_value_of_production_aes_adoption']) + total_aes_value_to_add
    income_model_output_df_extended.iloc[income_model_output_df_extended.loc[income_model_output_df_extended['ID_UZ'] == farm_id].index[0],2] = updated_value_to_assign #column 2 as this is always AES adoption value column


income_model_output_df_extended = income_model_output_df_extended.loc[income_model_output_df_extended['ID_UZ'].isin(farms_to_include_list)].reset_index()
income_model_output_df_extended_no_subsidies = income_model_output_df_extended_no_subsidies.loc[income_model_output_df_extended_no_subsidies['ID_UZ'].isin(farms_to_include_list)].reset_index()


print(income_model_output_df_extended) #visual error checking


# Guy Ziv's method: which 'IACS/LPIS' variables resemble the values feature-selected from FADN regressoin of AWU...

def area_crop_names_in_ha(farm_id, crop_names):
    
    total_crop_area_for_farm = []
    for name in crop_names:
        fields_concerned = crop_and_aes_values_table.loc[(crop_and_aes_values_table['ID_UZ'] == farm_id) & (crop_and_aes_values_table['crop'] == name)].reset_index() # usually locating witin 'farm_lpis_iacs_gdf' 'IACS/LPIS' directly #int/string issues here potentially
        for fields_i in range(0,len(fields_concerned)):
            if fields_concerned['crop_area'][fields_i] == 'nan':
                field_id = str(fields_concerned['NKOD_DPB'][fields_i])
                total_crop_area_for_farm += [float(farm_lpis_iacs_gdf.loc[farm_lpis_iacs_gdf['NKOD_DPB'] == field_id]['field_area_ha'])]
            else:
                total_crop_area_for_farm += [float(fields_concerned['crop_area'][fields_i])]
        
    total_crop_area_for_farm_summed = sum(total_crop_area_for_farm)
    return total_crop_area_for_farm_summed #in ha units



# FADN to 'IACS/LPIS' data conversion functions

def fetch_econ_size(farm_id, adoption_determination_string, subsidies_determination_string): #SE005
    
    if adoption_determination_string == "non-adoption": # econ size is dynamic as it itself depends on the AES adoption scenario...
        if subsidies_determination_string == "no subsidies":
            econ_size_estimate = float(income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_non_adoption'].loc[income_model_output_df_extended_no_subsidies['ID_UZ'] == farm_id]) / 1000.0 # divide by 1000 for units
        else:
            econ_size_estimate = float(income_model_output_df_extended['fnva_value_of_production_aes_non_adoption'].loc[income_model_output_df_extended['ID_UZ'] == farm_id]) / 1000.0 # divide by 1000 for units
    
    elif adoption_determination_string == "adoption":
        if subsidies_determination_string == "no subsidies":
            econ_size_estimate = float(income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_adoption'].loc[income_model_output_df_extended_no_subsidies['ID_UZ'] == farm_id]) / 1000.0 # divide by 1000 for units        
        else:
            econ_size_estimate = float(income_model_output_df_extended['fnva_value_of_production_aes_adoption'].loc[income_model_output_df_extended['ID_UZ'] == farm_id]) / 1000.0 # divide by 1000 for units
    
    else:
        print("Error in fetching economic size using 'IACS/LPIS': unrecognised 'adoption_determination_string' scenario")
        return None
    
    return econ_size_estimate

def fetch_total_uaa(farm_id): #SE025
    total_uaa_estimate = farm_lpis_iacs_gdf.loc[farm_lpis_iacs_gdf['ID_UZ'] == farm_id]['field_area_ha'].sum()
    return total_uaa_estimate

def fetch_rented_uaa(farm_id): #SE030 #assume 50% of farm area is rented for estimate here - should be more accurate on aggregate
    rented_uaa_estimate = 0.5 * farm_lpis_iacs_gdf.loc[farm_lpis_iacs_gdf['ID_UZ'] == farm_id]['field_area_ha'].sum()
    return rented_uaa_estimate

def fetch_cereals_area(farm_id): #SE035
    cereals_crop_type_names = ['wheat', 'maize', 'barley', 'rye', 'oats', 'triticale'] #ENTER VALUE HERE
    cereals_area_estimate = area_crop_names_in_ha(farm_id, cereals_crop_type_names)
    return cereals_area_estimate

def fetch_other_field_crops(farm_id): #SE041
    other_crop_type_names = ['potatoes','sugar beets'] #ENTER VALUE HERE
    other_field_crops_estimate = area_crop_names_in_ha(farm_id, other_crop_type_names)
    return other_field_crops_estimate

def fetch_energy_crops(farm_id): #SE042
    energy_crop_type_names = ['sugar beets', 'maize', 'rapeseed'] #ENTER VALUE HERE
    energy_crops_estimate = area_crop_names_in_ha(farm_id, energy_crop_type_names)
    return energy_crops_estimate

def fetch_vegetables_and_flowers(farm_id): #SE046
    vegetables_and_flowers_crop_type_names = ['vegetables', 'legumes', 'sunflower'] #ENTER VALUE HERE
    vegetables_and_flowers_estimate = area_crop_names_in_ha(farm_id, vegetables_and_flowers_crop_type_names)
    return vegetables_and_flowers_estimate

def fetch_vineyards(farm_id): #SE050
    vinyards_crop_type_names = ['vineyard'] #ENTER VALUE HERE
    vineyards_estimate = area_crop_names_in_ha(farm_id, vinyards_crop_type_names)
    return vineyards_estimate

def fetch_permanent_crops(farm_id): #SE054
    permanent_crops_crop_type_names = ['permanent crops', 'orchard'] #ENTER VALUE HERE
    permanent_crops_estimate = area_crop_names_in_ha(farm_id, permanent_crops_crop_type_names)
    return permanent_crops_estimate

def fetch_orchards(farm_id): #SE055
    orchards_crop_type_names = ['orchard'] #ENTER VALUE HERE
    orchards_estimate = area_crop_names_in_ha(farm_id, orchards_crop_type_names)
    return orchards_estimate

def fetch_olive_groves(farm_id): #SE060
    olive_groves_estimate = 0 #not in CZ 'IACS/LPIS'
    return olive_groves_estimate

def fetch_other_permanent_crops(farm_id): #SE065
    other_permanent_crops_estimate = 0 #not in CZ 'IACS/LPIS', judged to be a minimal influence
    return other_permanent_crops_estimate

def fetch_forage_crops(farm_id): #SE071
    forage_crops_crop_type_names = ['permanent grassland', 'legumes', 'other rutes and tubers', 'grass and clover'] #ENTER VALUE HERE
    forage_crops_estimate = area_crop_names_in_ha(farm_id, forage_crops_crop_type_names)
    return forage_crops_estimate

def fetch_set_aside(farm_id): #SE073
    set_aside_crop_type_names = ['set-aside'] #ENTER VALUE HERE
    set_aside_estimate = area_crop_names_in_ha(farm_id, set_aside_crop_type_names)
    return set_aside_estimate

def fetch_total_ag_area_out_of_production(farm_id): #SE074
    ag_out_of_production_crop_type_names = ['set-aside'] #ENTER VALUES HERE (for out-of-production crop names)
    total_ag_area_out_of_production_estimate = area_crop_names_in_ha(farm_id, ag_out_of_production_crop_type_names)
    return total_ag_area_out_of_production_estimate

def fetch_woodland_area(farm_id): #SE075
    woodland_area_estimate = 0 #not in CZ 'IACS/LPIS'
    return woodland_area_estimate

def fetch_farm_manager_age(farm_id): #AGE
    farm_manager_age_estimate = 0 #0 as not readily in 'IACS/LPIS'
    return farm_manager_age_estimate

def fetch_tf8(farm_id): #TF8
    econ_size_estimate = 0 #0 as not readily in 'IACS/LPIS'
    return econ_size_estimate

def fetch_farm_manager_sex(farm_id): #SEX
    farm_manager_sex_estimate = 0 #0 as not readily in 'IACS/LPIS'
    return farm_manager_sex_estimate

def fetch_region(farm_id): #REGION
    region_estimate = 0 #0 as not readily in 'IACS/LPIS'
    return region_estimate

def fetch_altitude(farm_id): #ALTITUDE
    altitude_estimate = 0 #0 as not readily in 'IACS/LPIS'
    return altitude_estimate

def fetch_anc(farm_id): #ANC
    anc_estimate = 0 #0 as not readily in 'IACS/LPIS'
    return anc_estimate


# Guy Ziv's method: which 'IACS/LPIS' variables resemble the values feature-selected from FADN regressoin of AWU...

import copy

# first, automatically retrieve the linear regression formula from the output dict from earlier, in non-disclosive manner

def retrieve_coef_table(filename):    
    ols_output_object = national_ols_model_summary_output_dict[filename[0:7] + "_OLS"]
    coef_table = []
    for i in range(1,len(ols_output_object.tables[1])): #begins at 1 to uninclude column headings, which are not needed
        coef_table += [[ols_output_object.tables[1].data[i][0],float(ols_output_object.tables[1].data[i][1].strip())]] #produces table of term (string) + coefficient (float) from statsmodels OLS regression output
    return coef_table


def generate_substituted_coef_table(coef_table, farm_id):
    
    global adoption_determination_string
    global subsidies_determination_string
    
    coef_table_substituted = coef_table
    # so this function must take an FADN variable from the OLS regression, then retrieve a method for how to calculate it for 'IACS/LPIS' using a given 'farm_id'
    # step 1: lookup method, step 2: within method (function), calculate value (...now aware of the actual inefficiencies arising here from the unfortunate fact that in fact *all* fetch functions being activated *every* time dict declared - this here minimises processing w/o re-structuring).
    # 'farm_id' and/or 'adoption_determination_string' declared here as relevant input into method dict values returned
    fadn_values_in_lpis_iacs_variables_translation_dict = {'SE005': fetch_econ_size(farm_id, adoption_determination_string, subsidies_determination_string),
                                                           'SE025': fetch_total_uaa(farm_id),
                                                           'SE030': fetch_rented_uaa(farm_id),
                                                           'SE035': fetch_cereals_area(farm_id),
                                                           'SE041': fetch_other_field_crops(farm_id),
                                                           'SE042': fetch_energy_crops(farm_id),
                                                           'SE046': fetch_vegetables_and_flowers(farm_id),
                                                           'SE050': fetch_vineyards(farm_id),
                                                           'SE054': fetch_permanent_crops(farm_id),
                                                           'SE055': fetch_orchards(farm_id),
                                                           'SE060': fetch_olive_groves(farm_id),
                                                           'SE065': fetch_other_permanent_crops(farm_id),
                                                           'SE071': fetch_forage_crops(farm_id),
                                                           'SE073': fetch_set_aside(farm_id),
                                                           'SE074': fetch_total_ag_area_out_of_production(farm_id),
                                                           'SE075': fetch_woodland_area(farm_id),
                                                           'AGE': fetch_farm_manager_age(farm_id),
                                                           'TF8': fetch_tf8(farm_id),
                                                           'SEX': fetch_farm_manager_sex(farm_id),
                                                           'REGION': fetch_region(farm_id),
                                                           'ALTITUDE': fetch_altitude(farm_id),
                                                           'ANC': fetch_anc(farm_id)} #started with 0 value, which will wipe out the component if returned, and build improvement where practical
    
    for i in range(1,len(coef_table_substituted)):
        coef_table_substituted[i][0] = fadn_values_in_lpis_iacs_variables_translation_dict[coef_table_substituted[i][0]] #generate_fadn_variable_value_using_lpis_iacs(coef_table_substituted[i][0], farm_id)
    return coef_table_substituted


def calculate_awu_estimate(coef_table_substituted):
    
    final_formula_awu_components_with_values = list()
    final_formula_awu_components_with_values += [coef_table_substituted[0][1]] #'coef_table_substituted[0][1]' is (the first and only) constant term, at index 0, thus...
    
    for i in range(1,len(coef_table_substituted)): #... begins at 1 here as begins the indexing in helpful way for the indexing of the nested list during the loop
        component_value_to_add = (coef_table_substituted[i][0] * coef_table_substituted[i][1])
        final_formula_awu_components_with_values += [component_value_to_add] #remember, adding list values here, as totals for each component in estimated reg. formula for AWU

    awu_estimate = sum(final_formula_awu_components_with_values)
    return awu_estimate



#replace NaN values in key area columns so can be detected in programme flow #https://stackoverflow.com/questions/19322506/pandas-dataframes-with-nans-equality-comparison
crop_and_aes_values_table = crop_and_aes_values_table.fillna(value={'crop_area': 'nan', 'AES_area': 'nan'}) #fills with a 'nan' string that can be used effectively in comparison operators    

# So, the task is to calculate an estimate for AWU per farm in the CS area (i.e. for each farm in both dataframes)
coef_table_to_input = retrieve_coef_table("CZE2017.csv") # this regression formula same for all farms in given CS...

awu_estimates_list_aes_non_adoption = []
awu_estimates_list_aes_adoption = []
awu_estimates_list_aes_non_adoption_no_subsidies = []
awu_estimates_list_aes_adoption_no_subsidies = []

farm_id_list = [] # error/assumption checking
for farm_id in list(income_model_output_df_extended['ID_UZ']): #list generation must be exact to the succession of the df row sequence here
    
    # HIST 1
    subsidies_determination_string = "subsidies"
    adoption_determination_string = "non-adoption"
    sub_coef_table_for_farm_aes_non_adoption = generate_substituted_coef_table(copy.deepcopy(coef_table_to_input), farm_id) #applying formula # 'deep copy' of list variable needed here to use as constant and prevent changes in each iteration to the 'formula' embedded list - copy() or [:] weren't working sufficiently (as 'shallow copies'?)
    awu_estimate_for_farm_aes_non_adoption = calculate_awu_estimate(sub_coef_table_for_farm_aes_non_adoption) #applying formula
    awu_estimates_list_aes_non_adoption += [awu_estimate_for_farm_aes_non_adoption] #add estimate for AWU labour for particular farm_id for **AES non-adoption** to become list to become column in 'income_model_output_df_extended'
    
    adoption_determination_string = "adoption"
    sub_coef_table_for_farm_aes_adoption = generate_substituted_coef_table(copy.deepcopy(coef_table_to_input), farm_id) #applying formula # 'deep copy' of list variable needed here to use as constant and prevent changes in each iteration to the 'formula' embedded list - copy() or [:] weren't working sufficiently (as 'shallow copies'?)
    awu_estimate_for_farm_aes_adoption = calculate_awu_estimate(sub_coef_table_for_farm_aes_adoption) #applying formula
    awu_estimates_list_aes_adoption += [awu_estimate_for_farm_aes_adoption] #add estimate for AWU labour for particular farm_id for **AES adoption** to become list to become column in 'income_model_output_df_extended'
    
    # HIST 2
    subsidies_determination_string = "no subsidies"
    adoption_determination_string = "non-adoption"
    sub_coef_table_for_farm_aes_non_adoption_no_subsidies = generate_substituted_coef_table(copy.deepcopy(coef_table_to_input), farm_id) #applying formula # 'deep copy' of list variable needed here to use as constant and prevent changes in each iteration to the 'formula' embedded list - copy() or [:] weren't working sufficiently (as 'shallow copies'?)
    awu_estimate_for_farm_aes_non_adoption_no_subsidies = calculate_awu_estimate(sub_coef_table_for_farm_aes_non_adoption_no_subsidies) #applying formula
    awu_estimates_list_aes_non_adoption_no_subsidies += [awu_estimate_for_farm_aes_non_adoption_no_subsidies] #add estimate for AWU labour for particular farm_id for **AES non-adoption** to become list to become column in 'income_model_output_df_extended'
    
    adoption_determination_string = "adoption"
    sub_coef_table_for_farm_aes_adoption_no_subsidies = generate_substituted_coef_table(copy.deepcopy(coef_table_to_input), farm_id) #applying formula # 'deep copy' of list variable needed here to use as constant and prevent changes in each iteration to the 'formula' embedded list - copy() or [:] weren't working sufficiently (as 'shallow copies'?)
    awu_estimate_for_farm_aes_adoption_no_subsidies = calculate_awu_estimate(sub_coef_table_for_farm_aes_adoption_no_subsidies) #applying formula
    awu_estimates_list_aes_adoption_no_subsidies += [awu_estimate_for_farm_aes_adoption_no_subsidies] #add estimate for AWU labour for particular farm_id for **AES adoption** to become list to become column in 'income_model_output_df_extended'
    
    
    farm_id_list += [farm_id] # for purposes of error checking assumptions on ordered lists etc.
    

    
    
# Then, reflect the standardisation by AWU in the FNVA estimate by here dividing the total FNVA revenue estimate by the estimate for AWU (adoption or non-adoption estimate) per farm in both dataframes (for adoption/non-adoption scenarios)

income_model_output_df_extended_no_subsidies['AWU_estimate_AES_non_adoption'] = awu_estimates_list_aes_non_adoption_no_subsidies #assumes farm_id location / ordered lists in Python
income_model_output_df_extended_no_subsidies['AWU_estimate_AES_adoption'] = awu_estimates_list_aes_adoption_no_subsidies #assumes farm_id location / ordered lists in Python
income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate'] = income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_non_adoption'] / income_model_output_df_extended_no_subsidies['AWU_estimate_AES_non_adoption']
income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate'] = income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_adoption'] / income_model_output_df_extended_no_subsidies['AWU_estimate_AES_adoption']

income_model_output_df_extended['AWU_estimate_AES_non_adoption'] = awu_estimates_list_aes_non_adoption #assumes farm_id location / ordered lists in Python
income_model_output_df_extended['AWU_estimate_AES_adoption'] = awu_estimates_list_aes_adoption #assumes farm_id location / ordered lists in Python
income_model_output_df_extended['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate'] = income_model_output_df_extended['fnva_value_of_production_aes_non_adoption'] / income_model_output_df_extended['AWU_estimate_AES_non_adoption']
income_model_output_df_extended['fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate'] = income_model_output_df_extended['fnva_value_of_production_aes_adoption'] / income_model_output_df_extended['AWU_estimate_AES_adoption']


# get rid of zeros, re-save results
income_model_output_df_extended_no_subsidies_no_zeros = income_model_output_df_extended_no_subsidies.loc[(income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate'] > 0.0) & (income_model_output_df_extended_no_subsidies['fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate'] > 0.0)][['index','ID_UZ','fnva_value_of_production_aes_non_adoption','fnva_value_of_production_aes_adoption','AWU_estimate_AES_non_adoption','AWU_estimate_AES_adoption','fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate','fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate']].reset_index()
income_model_output_df_extended_no_zeros = income_model_output_df_extended.loc[(income_model_output_df_extended['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate'] > 0.0) & (income_model_output_df_extended['fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate'] > 0.0)][['index','ID_UZ','fnva_value_of_production_aes_non_adoption','fnva_value_of_production_aes_adoption','AWU_estimate_AES_non_adoption','AWU_estimate_AES_adoption','fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate','fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate']].reset_index()


## APPENDIX 1: INVESTIGATING BIASES FROM EXCLUDED FARMS

farm_ids_to_investigate_biases = []
for farm_id in so_values_table['ID_UZ']:
    if farm_id not in income_model_output_df_extended_no_zeros['ID_UZ'].unique():
        farm_ids_to_investigate_biases += [farm_id]

inactive_ids_in_end_analysis = pd.DataFrame()
inactive_ids_in_end_analysis['farm_id_inactive'] = farm_ids_to_investigate_biases
inactive_ids_in_end_analysis.to_csv("czech_lpis/inactive_ids_table") #to import into QGIS for visual inspection of fields affected

farm_lpis_iacs_gdf_joined = farm_lpis_iacs_gdf.merge(inactive_ids_in_end_analysis, left_on = 'ID_UZ', right_on = 'farm_id_inactive', how = 'inner')

# all fields statistics
print("Mean average area of all fields:",farm_lpis_iacs_gdf['field_area_ha'].mean())
print("Median average area of all fields:",farm_lpis_iacs_gdf['field_area_ha'].median())
print("Maximum field area from all fields:",farm_lpis_iacs_gdf['field_area_ha'].min())
print("Minimum field area from all fields:",farm_lpis_iacs_gdf['field_area_ha'].max())
print("Total number of all fields:", len(farm_lpis_iacs_gdf))
print("Total area of all fields:", farm_lpis_iacs_gdf['field_area_ha'].sum())

# excluded fields statistics
print("Mean average area of (all) excluded fields:",farm_lpis_iacs_gdf_joined['field_area_ha'].mean())
print("Median average area of (all) excluded fields:",farm_lpis_iacs_gdf_joined['field_area_ha'].median())
print("Maximum field area from (all) excluded fields:",farm_lpis_iacs_gdf_joined['field_area_ha'].min())
print("Minimum field area from (all) excluded fields:",farm_lpis_iacs_gdf_joined['field_area_ha'].max())
print("Total number of (all) excluded fields:", len(farm_lpis_iacs_gdf_joined))
print("Total area of (all) excluded fields:", farm_lpis_iacs_gdf_joined['field_area_ha'].sum())

# comparative/combination statistics
print("Total field area included in the final results (percentage, 2 d.p.):", round(((farm_lpis_iacs_gdf['field_area_ha'].sum() - farm_lpis_iacs_gdf_joined['field_area_ha'].sum())/farm_lpis_iacs_gdf['field_area_ha'].sum()) * 100,2))
print("Total field count excluded in the final results (percentage, 2 d.p.):", round((len(farm_lpis_iacs_gdf_joined['field_area_ha'])/len(farm_lpis_iacs_gdf['field_area_ha'])) * 100,2))


## ENDING 1: D4.2 EXTRACTION

income_model_output_df_extended_no_zeros['percent_change_to_AES_adoption_4dp'] = round(((income_model_output_df_extended_no_zeros['fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate'] - income_model_output_df_extended_no_zeros['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate'])/income_model_output_df_extended_no_zeros['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate']) * 100,4) #rounded 4.d.p.
income_model_output_df_extended_no_zeros_concise = income_model_output_df_extended_no_zeros[['ID_UZ', 'percent_change_to_AES_adoption_4dp']]
income_model_output_df_extended_no_zeros_concise.to_csv("final_results_cz_tradeoff/south_moravia_data_fnva_income_change_table_south_moravia_czech_republic_with_subsidies_no_zeros.csv")


## ENDING 2: ORIGINAL CODE

# Combine back into new single dataframe, with two/three columns: 'farm_region' (optional, CS region and/or region within e.g. South Moravia etc.), 'FNVA_AES_adoption', 'FNVA_no_AES_adoption'
final_results_df_fnva_estimates_no_subsidies = pd.DataFrame(columns = ['farm_region', 'FNVA_no_AES_adoption', 'FNVA_AES_adoption'])
final_results_df_fnva_estimates_no_subsidies['FNVA_AES_adoption'] = income_model_output_df_extended_no_subsidies_no_zeros['fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate']
final_results_df_fnva_estimates_no_subsidies['FNVA_no_AES_adoption'] = income_model_output_df_extended_no_subsidies_no_zeros['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate']
final_results_df_fnva_estimates_no_subsidies['farm_region'] = 'South Moravia, CZE'

final_results_df_fnva_estimates_no_subsidies['percent_change_to_AES_adoption_4dp'] = round(((final_results_df_fnva_estimates_no_subsidies['FNVA_AES_adoption'] - final_results_df_fnva_estimates_no_subsidies['FNVA_no_AES_adoption'])/final_results_df_fnva_estimates_no_subsidies['FNVA_no_AES_adoption']) * 100,4) #rounded 4.d.p.
#final_results_df_fnva_estimates_no_subsidies.describe().to_csv("final_results_cz/results_summary_statistics_describe_table_south_moravia_czech_no_subsidies_no_zeros_final.csv")


# Combine back into new single dataframe, with two/three columns: 'farm_region' (optional, CS region and/or region within e.g. South Moravia etc.), 'FNVA_AES_adoption', 'FNVA_no_AES_adoption'
final_results_df_fnva_estimates = pd.DataFrame(columns = ['farm_region', 'FNVA_no_AES_adoption', 'FNVA_AES_adoption'])
final_results_df_fnva_estimates['FNVA_AES_adoption'] = income_model_output_df_extended_no_zeros['fnva_value_of_production_aes_adoption_standardised_by_AWU_estimate']
final_results_df_fnva_estimates['FNVA_no_AES_adoption'] = income_model_output_df_extended_no_zeros['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate']
final_results_df_fnva_estimates['farm_region'] = 'South Moravia, CZE'

final_results_df_fnva_estimates['percent_change_to_AES_adoption_4dp'] = round(((final_results_df_fnva_estimates['FNVA_AES_adoption'] - final_results_df_fnva_estimates['FNVA_no_AES_adoption'])/final_results_df_fnva_estimates['FNVA_no_AES_adoption']) * 100,4) #rounded 4.d.p.
final_results_df_fnva_estimates.describe().to_csv("final_results_cz/results_summary_statistics_describe_table_south_moravia_czech_with_subsidies_no_zeros_final.csv")


# BOXPLOTS
plt.figure(figsize=(8,8))
#plt.rcParams['figure.dpi'] = 150 #https://blakeaw.github.io/2020-05-25-improve-matplotlib-notebook-inline-res/
#plt.rcParams['savefig.dpi'] = 150 #https://blakeaw.github.io/2020-05-25-improve-matplotlib-notebook-inline-res/
n_value = len(final_results_df_fnva_estimates)
final_results_df_fnva_estimates.boxplot(column=['FNVA_no_AES_adoption','FNVA_AES_adoption'], grid=False, color='b', showfliers=False); #https://stackoverflow.com/questions/22028064/matplotlib-boxplot-without-outliers
plt.ylabel("Value of Agricultural Production [Gross Income / Total Revenue, part of FNVA] (€)\n")
plt.xlabel("\nData Acknowledgements: FADN data in methodology for CZE (2017) from Directorate-General for Agriculture and Rural Development,\n European Commission. 'IACS/LPIS' data (2017) from Ministry of Agriculture of the Czech Republic.", size = 8)
plt.xticks([1,2],["No AES adoption\nscenario", "AES adoption\nscenario"])
plt.title("Boxplots of distributions for estimated 'Value of Agricultural Production' values (€)\n for Agri-Environment Scheme ('AES') non-adoption and adoption scenarios. \nSouth Moravia, Czech Republic, 2017 (n="+str(n_value)+") [Data: FADN,'IACS/LPIS'].\n[€0 income farms removed, outliers not displayed]\n", size = 12);
plt.savefig("final_results_cz/boxplot_cz_farm_income_model_output_no_zeros_no_outliers_final.png", bbox_inches='tight')


# HISTOGRAM: PERCENT CHANGE FULL DISTRIBUTION FIGURE

import seaborn as sns
plt.rcParams['figure.dpi'] = 150 #https://blakeaw.github.io/2020-05-25-improve-matplotlib-notebook-inline-res/
plt.rcParams['savefig.dpi'] = 150 #https://blakeaw.github.io/2020-05-25-improve-matplotlib-notebook-inline-res/

n_value = len(final_results_df_fnva_estimates)

min_number_farms_per_bin_total_to_2dp = 4

plt.figure(figsize=(10.0,5.0))
sns.displot(data=final_results_df_fnva_estimates['percent_change_to_AES_adoption_4dp'], stat='probability', bins=(int((n_value/min_number_farms_per_bin_total_to_2dp)-1)), color = 'black');
from matplotlib.ticker import PercentFormatter
plt.gca().yaxis.set_major_formatter(PercentFormatter(1))
plt.gca().xaxis.set_major_formatter(PercentFormatter(100))
plt.xlim(int((final_results_df_fnva_estimates['percent_change_to_AES_adoption_4dp'].min()) - 2), int((final_results_df_fnva_estimates['percent_change_to_AES_adoption_4dp'].max()) + 2)) #SDC protecting automatic
plt.ylim(0,) #percentage of sample
plt.xlabel("\nEstimated percent difference in FNVA from adopting AES (%),\n" + str(round(1/min_number_farms_per_bin_total_to_2dp,2)) + " >= total bins per farm in sample (to 2 d.p.)\n\n\nData Acknowledgements: FADN data in methodology for CZE (2017) from Directorate-General\n for Agriculture and Rural Development, European Commission.'IACS/LPIS' data (2017)\n from Ministry of Agriculture of the Czech Republic.")
plt.ylabel("Percent of farm sample (%)\n")
plt.title("Proportion of farm sample (n = " + str(n_value) + ") exhibiting (approximate) estimate percent \ndifference in Farm Net Value Added ('FNVA', C.27 PMEF) from adopting AES.\nSouth Moravia, Czech Republic, 2017 [Data: FADN, 'IACS/LPIS'].\n[€0 income farms removed]\n", fontsize= 12)
plt.savefig("final_results_cz/histogram_cz_farm_income_model_output_with_subsidies_no_zeros_final.png", bbox_inches ='tight')


# SCATTERPLOT

import seaborn as sns
plt.rcParams['figure.dpi'] = 150 #https://blakeaw.github.io/2020-05-25-improve-matplotlib-notebook-inline-res/
plt.rcParams['savefig.dpi'] = 150 #https://blakeaw.github.io/2020-05-25-improve-matplotlib-notebook-inline-res/

value_pairs_to_plot_x, value_pairs_to_plot_y, value_pairs_to_plot_z = [], [], []
for value in range(0,15000,200):
    value_pairs_to_plot_x += [value]
    value_pairs_to_plot_y += [income_model_output_df_extended_no_zeros['percent_change_to_AES_adoption_4dp'].loc[income_model_output_df_extended_no_zeros['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate'] >= value].mean()]
    value_pairs_to_plot_z += [len(income_model_output_df_extended_no_zeros.loc[income_model_output_df_extended_no_zeros['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate'] >= value])]

q1_q2_q3_non_aes_adoption_income_list = list(income_model_output_df_extended_no_zeros['fnva_value_of_production_aes_non_adoption_standardised_by_AWU_estimate'].describe().values[4:7])

df_scatterplot_values = pd.DataFrame()
df_scatterplot_values['threshold_value_for_non_aes_income_to_qualify_for_inclusion'] = value_pairs_to_plot_x
df_scatterplot_values['mean_percent_income_change_upon_aes_adoption'] = value_pairs_to_plot_y
df_scatterplot_values['sample_size_at_threshold'] = value_pairs_to_plot_z

plt.figure(figsize = [8,4])
sns.scatterplot(data = df_scatterplot_values, x = 'threshold_value_for_non_aes_income_to_qualify_for_inclusion', y = 'mean_percent_income_change_upon_aes_adoption', hue = 'sample_size_at_threshold', marker = 'o');
plt.title("Filtering sensitivity: relationship between threshold for income from non-AES adoption for eligibility for inclusion \nin the analysis and the respective cohort mean average for estimated percent change in FNVA upon AES adoption\nSouth Moravia, Czech Republic, 2017 (Maximum n = " + str(value_pairs_to_plot_z[0]) + ") [Grey lines at Q1, Q2, Q3: AES non-adoption income]\n", size = 8)
plt.xlabel("\nThreshold for inclusion in analysis: AES non-adoption estimated Value of Agricultural Production (€ per AWU)", size = 8)
plt.ylabel("Mean est. percent change in FNVA (%)\n", size = 8)
plt.legend(title = 'Sample (n)')
plt.xlim(-250,) #-250 instead of zero for visibility purposes
plt.axvline(x = q1_q2_q3_non_aes_adoption_income_list[0], color = 'gray', linewidth = 0.8) #Q1 line
plt.axvline(x = q1_q2_q3_non_aes_adoption_income_list[1], color = 'gray', linewidth = 0.8) #Q2 line
plt.axvline(x = q1_q2_q3_non_aes_adoption_income_list[2], color = 'gray', linewidth = 0.8); #Q3 line
plt.savefig("final_results_cz/scatterplot_fnva_sensitivity_plotted.png", bbox_inches = 'tight')